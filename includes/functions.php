<?php
	if (!defined("_PROFIL_ACCESS")){header('location:../erreur');}
	
	$dicoFile = _VHOST_PATH . "traductions/" . $_SESSION['locale'] . ".inc";
	$_dico="";
	if (file_exists($dicoFile))
	{   
		$contenu_array = file($dicoFile);
		$num_line=sizeof($contenu_array);
		while ( list( $numero_ligne, $ligne ) = each( $contenu_array ) ) 
		{
			$var=explode("=", $ligne);
			$_dico[$var[0]]=rtrim(str_replace("::","=",$var[1]));
		}
	}
	
	
	function add_param($url, $param)
	{
		$url = strpos($url, "?") === false ? $url ."?" . $param : $url ."&" . $param ;
		return $url;
	}
	
	function is_logged()
	{
		if($_SESSION['access'] != "logged")
		{
			header('location:accueil');
		}
	}
	
	function infos_connexion()
	{
		$connexion = '<ul>';
		if($_SESSION['user']['groupe'] == "PUBLIC" ){
			$connexion .= '<li class="inbl pl3"><a href="login" id="login_link">M\'identifier</a></li></li>';
		}
		elseif($_SESSION['access'] == "logged")
		{
			$connexion .= '<li class="inbl pl3">Bonjour ' . $_SESSION['user']['prenom']. ' ' . $_SESSION['user']['nom'] . ' | <a href="mon_compte">Mon compte</a></li><li class="inbl pl3"><a href="logout">Déconnexion</a></li>';
		}
		
		$connexion .= '<li class="inbl pl3 pr3"><a href="a-propos">À propos</a></li></ul>';
		
		return $connexion;
	}
	
	function log_user($user_data, $provider)
	{
		global $T01, $T14;
		
		$login = $T14->getCompteByEmail($user_data->email, $provider);
		
		$_SESSION['user']['id_user'] = $login[0]->T01_codeinterne_i;
		$_SESSION['user']['id_user_externe'] = $user_data->identifier;
		$_SESSION['user']['login'] = $user_data->email;
		$_SESSION['user']['avatar'] = $user_data->photoURL;
		$_SESSION['user']['nom']= $user_data->lastName;
		$_SESSION['user']['prenom'] = $user_data->firstName;
		$_SESSION['user']['email'] = $user_data->email;
		$_SESSION['user']['adresse']['adresse'] = $user_data->address;
		$_SESSION['user']['adresse']['cp'] = $user_data->zip;
		$_SESSION['user']['adresse']['ville'] = $user_data->city;
		$_SESSION['user']['adresse']['pays'] = $user_data->country == null ? "France" : $adresse->country;
		
		$_SESSION['user']['adresse']['latitude'] = 48.8675652;
		$_SESSION['user']['adresse']['longitude'] = 2.3439901;
		$_SESSION["user"]["provider"] = $provider;

		
		if(count($login) > 0)
		{
			$_SESSION['user']['role'] = "MEMBRE";
			$_SESSION['user']['groupe'] = "MEMBRE";
			$_SESSION['access'] = "logged";
			return true;
		}
		else
		{
			$_SESSION['access'] = "";
			$_SESSION['user']['role'] = "PUBLIC";
			$_SESSION['user']['groupe'] = "PUBLIC";
			if(_INSCRIPTION_MODE == "open")
			{
				$t01_codeinterne_i = $T01->insert(2, $_SESSION['user']['id_user_externe'] , $_SESSION['user']['nom'], $_SESSION['user']['prenom'], $_SESSION['user']['email'], $_SESSION['user']['adresse']['ville'], $_SESSION['user']['adresse']['cp'], $_SESSION['user']['login'], now(), "fr", $_SESSION["user"]["provider"]);
				
				$_SESSION['user']['id_user'] = $t01_codeinterne_i;
				$_SESSION['user']['role'] = "MEMBRE";
				$_SESSION['user']['groupe'] = "MEMBRE";
				$_SESSION['access'] = "logged";
			}
			else
			{
				header("location:login?error=oAuth&provider={$provider}");
				exit;
			}
			return false;
		}
	}
	function _T($libelle , $variables = array())
	{
		GLOBAL $_dico;
		$trad = $_dico[$libelle];
		foreach ($variables as $name => $value)
		{
			$trad = preg_replace("/@" . $name . "@/i", $value, $_dico[$libelle]);
		}
	
		return $trad;
	}
	
	function array_change_value_case(array $input, $case = CASE_LOWER) { 
    switch ($case) { 
        case CASE_LOWER: 
            return array_map('mb_strtolower', $input); 
            break; 
        case CASE_UPPER: 
            return array_map('mb_strtoupper', $input); 
            break; 
        default: 
            trigger_error('Case is not valid, CASE_LOWER or CASE_UPPER only', E_USER_ERROR); 
            return false; 
    } 
} 

	function nettoie_chaine($chaine, $separateur=" ")
	{
		$caracteres = array(
				'À' => 'a', 'Á' => 'a', 'Â' => 'a', 'Ä' => 'a', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ä' => 'a', '@' => 'a',
				'È' => 'e', 'É' => 'e', 'Ê' => 'e', 'Ë' => 'e', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', '€' => 'e',
				'Ì' => 'i', 'Í' => 'i', 'Î' => 'i', 'Ï' => 'i', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
				'Ò' => 'o', 'Ó' => 'o', 'Ô' => 'o', 'Ö' => 'o', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'ö' => 'o',
				'Ù' => 'u', 'Ú' => 'u', 'Û' => 'u', 'Ü' => 'u', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'µ' => 'u',
				'Œ' => 'oe', 'œ' => 'oe',
				'$' => 's');
				
		$chaine = strtr($chaine, $caracteres);
		$chaine = preg_replace('#[^A-Za-z0-9]+#', $separateur, $chaine);
				
		return $chaine;
	}
	
	function isort($a,$b) {
		return strtolower($a)>strtolower($b);
	}

	function roundDown($value, $prec)
	{
		return intval($value * puissance(10, $prec) ) / puissance(10, $prec);
	}
	
	function puissance($x,$y) { 
	  $resultat=1;
	  for ($i=0;$i<$y;$i++)
	   $resultat *= $x;
	  return $resultat;
	}
	
	function now($mode)
	{
		$now = getdate(); 
		if($mode == "date") { $format = "%02d/%02d/%04d";}
		if($mode == "datetime") { $format = "%02d/%02d/%04d %02d:%02d:%02d";}
		$today = sprintf($format, $now['mday'], $now['mon'], $now['year'], $now['hours'], $now['minutes'], $now['seconds']);
		return $today;	
	}
	
	function mysql2frDate($date, $type="date")
	{
		$_D = explode(' ', $date);
		$_T = $_D[1];
		$_D = explode('-', $_D[0]);
		switch($type)
		{
			default;
			
			case "date":
				return $_D[2]."/".$_D[1].'/'.$_D[0];
				break;
				
			case "datetime":
				return $_D[2]."/".$_D[1].'/'.$_D[0] . " " . $_T;
				break;
		}
	}
	
	function fr2mysqlDate($date)
	{
		$_D = explode('/', $date);
	
		return $_D[2]."-".$_D[1].'-'.$_D[0];
	}
	
	function raccourcir($string, $length)
	{
		if(mb_strlen($string) <= $length)
		{
			return $string;
		}
		else
		{
			return mb_substr($string, 0, $length)."...";
		}
	}
	
	function NumTel($tel) 
	{ 
		$ch = 10;                               // Numéro à 10 chiffres 
		$tel = eregi_replace('[^0-9]',"",$tel); // supression sauf chiffres 
		$tel = trim($tel);                      // suppression espaces avant et après 
		if (strlen($tel) > $ch) 
		{ 
			$d = strlen($tel) - $ch; // retrouve la position pour ne garder 
									 // que les $ch derniers 
		} 
		else 
		{ 
			$d = 0; 
		} 
		$tel = substr($tel,$d,$ch); // récupération des $ch derniers chiffres 
		$regex = '([0-9]{1,2})([0-9]{1,2})([0-9]{1,2})([0-9]{1,2})([0-9]{1,2})$'; 
		$newtel = eregi_replace($regex, 
			'\\1 \\2 \\3 \\4 \\5',$tel); // mise en forme 
		return $newtel; /* Exemple : 03-81-51-45-78  */ 
	}

	function NumSiret($tel) 
	{ 
		$ch = 14;                               // Numéro à 10 chiffres 
		$tel = eregi_replace('[^0-9]',"",$tel); // supression sauf chiffres 
		$tel = trim($tel);                      // suppression espaces avant et après 
		if (strlen($tel) > $ch) 
		{ 
			$d = strlen($tel) - $ch; // retrouve la position pour ne garder 
									 // que les $ch derniers 
		} 
		else 
		{ 
			$d = 0; 
		} 
		$tel = substr($tel,$d,$ch); // récupération des $ch derniers chiffres 
		$regex = '([0-9]{1,3})([0-9]{1,3})([0-9]{1,3})([0-9]{1,5})$'; 
		$newtel = eregi_replace($regex, 
			'\\1 \\2 \\3 \\4',$tel); // mise en forme 
		return $newtel; /* Exemple : 03-81-51-45-78  */ 
	}
	
	function formtel($tel)
	{
		$tel = str_replace(" ", "", $tel);
		$tel = str_replace("/", "", $tel);
		$tel = str_replace("-", "", $tel);
		
		return $tel;
	}

	function formPDF($str)
	{
		for($k = 0; $k < strlen($str); $k++)
		{
			$l=substr($str, $k, 1);
			$val .= $l." ";
		}
		
		return trim($val);
	}
	
	function flatArray($array)
	{
		ob_start();
		print_r($array);
		$text = ob_get_contents();
		ob_end_clean();
		
		return $text;
	}
	function make_block($text, $className)
	{
		return '<div class="' . $className . '"><div class="box-top png  margin-top-block"></div><div class="box-content"><div class="content">' . $text . '</div><br class="nettoyeur" /></div><div class="box-bottom png"></div><br class="nettoyeur" style="margin-bottom:20px;"/></div>';
	}
	
	function ecranXss($val)
	{
		$val = strip_tags($val);
		$val = htmlspecialchars($val, ENT_COMPAT, "utf-8");
		return $val;
	}
	
	
	function load_metas()
	{
		/*
		$xml = new DomDocument();
		$xml->preserveWhiteSpace = false;
		try {
			$xml->load(_VHOST_URL  . '/metas.xml');
			$xml->formatOutput = true;
			$racine = $xml->documentElement;
			
			$oPages = $racine->getElementsByTagName("page");
			foreach ($oPages as $oPage)
			{
				$page_id = $oPage->getAttribute('page_id');
				
				$oMetas = $oPage->getElementsByTagName("meta");
				
				foreach($oMetas as $oMeta)
				{
					$metas[$page_id][$oMeta->getAttribute('name')] = $oMeta->nodeValue;
				}
			}
		}
		 catch (Exception $e) {
		    error_log('Exception reçue : ' .  $e->getMessage());
		}
		return $metas;
		*/
	}
	
	function controleLuhn($val){
	   $val        = preg_replace('/[^0-9]/', '', $val);
	   $len        = strlen($val);
	   $sum        = 0;
	   for($i = 0; $i < $len; $i++){
		  $indice        = ($len - $i); // donne l'indice en partant de la fin indice de 1 à n
		  /*
		  $indice impaire :  $indice%2 => 1, 2 - 1 = 1,
		  $indice paire : $indice%2 => 0, 2 - 0 =  2
		  */
		  $tmp         = (2 - ($indice%2)) * $val[$i];
		  // si nb à 2 chiffres, somme des 2 chiffres 11 => 2, 12 => 3
		  if($tmp >= 10) $tmp -= 9;
		  $sum       += $tmp;
	   }
	   return (($sum%10) == 0);
	}
?>