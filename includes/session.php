<?php
/*
 *  Gestion des profils
 *  @author: Thierry Cazalet
 *
 */

if (!defined("_PROFIL_ACCESS")){header('location:../erreur');}

	
function verifLogin($conn, $login, $password, $referer="index.php")
{
	// On instancie la classe T01_compte
    $T01 = new T01_users($conn);
	$users = $T01->getCompteByLogin($login);
	$user = $users[0];
	
	if ($user->T01_password_va == md5($password))
	{
		$_SESSION['user']['id_user'] = $user->T01_codeinterne_i;
		$_SESSION['user']['login'] = $login;
		$_SESSION['user']['avatar'] = "";
		$_SESSION['user']['role'] = $user->T08_valeur_va;
		$_SESSION['user']['groupe'] = $user->T02_role_va;
		$_SESSION['user']['roleID'] = $user->T02_codeinterne_i;
		$_SESSION['user']['nom']= $user->T01_nom_va;
		$_SESSION['user']['prenom'] = $user->T01_prenom_va;
		$_SESSION['user']['email'] = $user->T01_email_va;
		$_SESSION['user']['telephone'] = $user->T01_telephone_va;
		//$_SESSION['user']['adresse']['adresse'] = $user->T01_adresse_va;
		$_SESSION['user']['adresse']['cp'] = $user->zip;
		$_SESSION['user']['adresse']['ville'] = $user->city;
		$_SESSION['user']['adresse']['pays'] = $user->country == null ? "France" : $adresse->country;

		
		$_SESSION['user']['lastlog'] = $user->T01_lastlog_d;
		$_SESSION['locale'] =  $user->T01_locale_va;
		$_SESSION["user"]["provider"] = "internal";
		
		$_SESSION["user"]["modele_facture"] = "facture_1";
		$_SESSION["user"]["modele_devis"] = "devis_1";
		$_SESSION["user"]["modele_avoir"] = "avoir_1";
		
		$_SESSION['user']['logo'] = _ASSET_PATH . "logo/" . $user->T01_logo_va;
		$_SESSION['user']['cgv'] = _ASSET_PATH . "cgv/" . $user->T01_codeinterne_i . "-cgv.pdf";
		
		$T01->updateDateConnection($user->T01_codeinterne_i);
		
		$_SESSION['access'] = "logged";
	}
	else
	{
		header('location:erreur');
		
	}
	
	
}

?>