<?php
	if (!defined("_PROFIL_ACCESS")){header('location:../erreur');}
	$include_dir = dirname(__FILE__);
	
	$path = substr($include_dir , 0, strlen($include_dir) - strlen("includes"));
	define('_VHOST_PATH', $path);
	
	$hw_path = substr($path , 0, strlen($path) - strlen(_APP_MODULE));
	define('_VHOST_HB_PATH', $hw_path);
	define ('_ASSET_PATH', _VHOST_PATH . "assets/");
	
	// Définition du fuseau horaire
	date_default_timezone_set('Europe/Paris');
	$_SESSION['locale'] = "FR";
	
	
	ini_set("session.bug_compat_42" , 0);
	ini_set('mbstring.language', 'UTF-8');
	ini_set('mbstring.internal_encoding', 'UTF-8');
	ini_set('mbstring.http_input', 'UTF-8');
	ini_set('mbstring.http_output', 'UTF-8');
	ini_set('mbstring.detect_order', 'auto');

	$mainDir = "templates/";
	$templateDir = "templates/";
	
	include_once(_VHOST_PATH . 'includes/config.php');
    include_once(_VHOST_PATH . 'includes/connection.php');
	include_once(_VHOST_PATH . 'includes/functions.php');
	include_once(_VHOST_PATH . 'includes/session.php');
	include_once(_VHOST_PATH . 'sections/menu_principal.php');

	include_once(_VHOST_PATH . 'classes/db/class.db.php');
	include_once(_VHOST_PATH . 'classes/db/class.queryException.php');
	include_once(_VHOST_PATH . 'classes/db/class.t01_users.php');
	include_once(_VHOST_PATH . 'classes/db/class.t02_role.php');
	include_once(_VHOST_PATH . 'classes/db/class.t03_clients.php');
	include_once(_VHOST_PATH . 'classes/db/class.t04_contacts.php');
	include_once(_VHOST_PATH . 'classes/db/class.t05_pieces_comptables.php');
	include_once(_VHOST_PATH . 'classes/db/class.t06_type_piece.php');
	include_once(_VHOST_PATH . 'classes/db/class.t07_type_article.php');
	include_once(_VHOST_PATH . 'classes/db/class.t08_articles.php');
	include_once(_VHOST_PATH . 'classes/db/class.t09_taux_tva.php');
	include_once(_VHOST_PATH . 'classes/db/class.t10_dico.php');
	include_once(_VHOST_PATH . 'classes/db/class.t11_numeros_factures.php');
	include_once(_VHOST_PATH . 'classes/db/class.t12_pieces_status.php');

	include_once(_VHOST_PATH . 'classes/class.template.php');
	include_once(_VHOST_PATH . 'classes/class.phpmailer.php');
	include_once(_VHOST_PATH . 'classes/class.utils.php');
	include_once(_VHOST_PATH . 'classes/class.log.php');
	include_once(_VHOST_PATH . 'classes/class.validate.php');
	include_once(_VHOST_PATH . 'classes/class.formulaire.php' );

	$provider = ""; 

	$metas = load_metas();
	$page_id = "accueil";

	$T01 = new t01_users($conn);
	$T02 = new t02_role($conn);
	$T03 = new t03_clients($conn);
	$T04 = new t04_contacts($conn);
	$T05 = new t05_pieces_comptables($conn);
	$T06 = new t06_type_piece($conn);
	$T07 = new t07_type_article($conn);
	$T08 = new t08_articles($conn);
	$T09 = new t09_taux_tva($conn);
	$T10 = new t10_dico($conn);
	$T11 = new t11_numeros_factures($conn);
	$T12 = new t12_pieces_status($conn);

	$log = new Log($conn);

	$_SESSION['debug'] = $_GET['debug'];
	$_SESSION['maintenance'] = "off";
	
	if(!isset($_SESSION['user']['groupe'])){$_SESSION['user']['groupe'] = "PUBLIC";}
?>