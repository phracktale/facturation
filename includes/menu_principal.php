<?php

 if (!defined("_PROFIL_ACCESS")){header('location:../erreur');}
 
	$menu = array (
					"ADMIN" => 	array (
													"item1"		=> array( "code" => "accueil", "class" => "", "libelle" => _T('libMenuAccueil'))
													
										),
					"PUBLIC" => 	array (
													"item1"		=> array( "code" => "recruteur", "class" => "orange", "libelle" => _T('libMenuRecruteur')),
													"item2"		=> array( "code" => "offreService", "class" => "violet", "libelle" => _T('libOffreService'))
										),
					"MEMBRE" => 	array (
													"item1"		=> array( "code" => "recruteur", "class" => "orange", "libelle" => _T('libMenuRecruteur')),
													"item2"		=> array( "code" => "offreService", "class" => "violet", "libelle" => _T('libOffreService'))
										)
					
			);
			
	
	
	if(isset($_SESSION['user']['groupe']))
	{
		
		$menu_principal = '<nav id="menu">';
		$menu = $menu[$_SESSION['user']['groupe']];
		if(count($menu) > 0)
		{
			$menu_principal = '<ul>';
			foreach ($menu as $item)
			{
				$active = $item['code'] == $_SESSION['contexte']['rubrique'] ? "on" : "off";
				$class = $item['class'] . "_" . $active;
				$menu_principal .= '<li class="' . $item['code'] . ' ' . $class . '"><a href="index.php?rubrique=' . $item['code'] . '" class="button"><span class="lib">' . $item['libelle'] . '</span></a></li>';
			}
			$menu_principal .= '</ul>';
		}
		$menu_principal .= '</nav>';
	}
?>