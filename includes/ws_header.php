<?php
/*
   * Preparation des header (appel au web service)
   *
   * utilisation : 
   * $headers = prepareWsseHeaders($username, $password);
   * foreach($headers as $key => $value) {
   *   header($key.': '.$value);
   * }
  * 
  */
     function dj_prepareWsseHeaders($username, $password) {
         $apiData = array(
             'Username' => $username,
             'Created' => date('c'),
             'Nonce' => base64_encode(sha1(mt_rand().mt_rand(), true)),
        );
         $apiData['PasswordDigest'] = base64_encode(sha1($apiData['Nonce'].$apiData['Created'].$password, true));
         $usernameToken = 'UsernameToken ';
         $loop = false;
         foreach($apiData as $key => $value) {
             $usernameToken .= ($loop?', ':'').$key.'="'.$value.'"';
             if($loop === false) $loop = true;
         }
		 
        $wsse_header[] = 'Authorization: WSSE profile "UsernameToken"';
		$wsse_header[] = 'X-WSSE: ' . $usernameToken;
		
         return $wsse_header;
     }
      
?>