<html>
	<head>
		<style type="text/css">
			body {font-family:verdana; font-size:0.7em;}
		</style>
	</head>
	<body>
	<?php
	echo "<h1>Génération du squelette de l'application</h1>";
	$application = new DomDocument();
	$application->load("application.xml");
	$roles = $application->getElementsByTagName('role');
	
	/* debut ROLES */
	if(count($roles)>0)
	{
		echo "<ul>";
		
		/* debut foreach ROLES */
		foreach ($roles as $role)
		{
			$FW_ROLE = $role->getAttribute('name');
			echo "<li><h2>" . $FW_ROLE . "</h2></li>";
			
/* DEBUT SECTION PREMIER NIVEAU FRAGMENT 1*/			
			$ROLE_FILE = '<?php
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	## Profil ' . $FW_ROLE . '
	#
	#	Routage des fonctionnalités du role ' . $FW_ROLE . '
	#
	#	Attention ! Ce fichier ne doit jamais être édité
	
	$soustitre = "";
	
	switch($_SESSION["contexte"]["rubrique"])
	{
		default:';
/* FIN SECTION PREMIER NIVEAU FRAGMENT 1*/						
			
			
			$rubriques = $role->getElementsByTagName('rubrique');
			/* debut RUBRIQUES */
			if(count($rubriques)>0)
			{
				echo "<ul>";
				
/* DEBUT MENU ITEMS FRAGMENT 1 */	
				$MENU_ITEMS .= '
				"' . $FW_ROLE . '" => 	array (';
/* FIN MENU ITEMS FRAGMENT 1 */	
				$k_item = 1;
				
				/* debut foreach RUBRIQUES */
				foreach ($rubriques as $rubrique)
				{
					$FW_RUBRIQUE = $rubrique->getAttribute('name');
					$FW_TITRE = $rubrique->getAttribute('titre');
					echo "<li><h3>" . $FW_RUBRIQUE . "</h3></li>";
					
/* DEBUT MENU ITEMS FRAGMENT 2 */	
					
					$MENU_ITEMS .= '
											"item' . $k_item . '"	=> array( 
																"code" => "' . $FW_RUBRIQUE . '", 
																"class" => "", 
																"libelle" => _T("libMenu' . ucfirst(strtolower($FW_RUBRIQUE)) . '")';
					/* Recherche sousnav */										
					$fonctions = $rubrique->getElementsByTagName('fonction');
					if(count($fonctions)>0)
					{
						$sousmenu = "";
						$k2_item = 1;
						foreach ($fonctions as $fonction)
						{
							if($fonction->getAttribute('sousmenu')=="true")
							{
								$FW_FONCTION = $fonction->getAttribute('name');
								$sousmenu .= '													"item' . $k_item . '-' . $k2_item . '" => array( "code" => "' . $FW_FONCTION . '", "class" => "", "libelle" => _T("libMenu' . ucfirst(strtolower($FW_RUBRIQUE)) . ucfirst(strtolower($FW_FONCTION)) . '")),
								';
								$k2_item++;
							}
						}
					
						if(count($sousmenu) > 0)
						{
							$sousmenu = rtrim($sousmenu, ",");
							$MENU_ITEMS .= ',
																"sousmenu" => array(
							' . $sousmenu . '						
															)';
						}
					}
					$MENU_ITEMS .= '
													),';
					$k_item++;
/* FIN MENU ITEMS FRAGMENT 2 */	

/* DEBUT SECTION PREMIER NIVEAU FRAGMENT 2 */						
					
					$ROLE_FILE .= '
		case "' . $FW_RUBRIQUE . '":
			$titre = "'  . $FW_TITRE . '";
			include("' . $FW_ROLE . '_' . $FW_RUBRIQUE . '.php");
			break;
			';
/* FIN SECTION PREMIER NIVEAU FRAGMENT 2 */	

/* DEBUT SECTION DEUXIEME NIVEAU FRAGMENT 1 */	
					$RUBRIQUE_FILE = '<?php
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	
	//switch($_SESSION["contexte"][$_SESSION["contexte"]["rubrique"]]["page"])
	switch($_GET["page"])
	{
		default:
		';
/* FIN SECTION DEUXIEME NIVEAU FRAGMENT 1 */	
					
					$fonctions = $rubrique->getElementsByTagName('fonction');
					if(count($fonctions)>0)
					{
						 echo "<ul>";
						/* debut foreach FONCTIONS */
						foreach ($fonctions as $fonction)
						{
							$FW_FONCTION = $fonction->getAttribute('name');
							if($fonction->getAttribute('case') != "")
							{
								$FW_FONCTION_DEFAULT = "<small>[" . $fonction->getAttribute('case') ."]</small>";
							}
							else
							{
								$FW_FONCTION_DEFAULT = "";
							}
							
							echo "<li><h3>" . $FW_FONCTION . " " . $FW_FONCTION_DEFAULT . "</h3> </li>";

/* DEBUT SECTION DEUXIEME NIVEAU FRAGMENT 2 */								
							$RUBRIQUE_FILE .='
		case "' . $FW_FONCTION . '":
			include("function_' . $FW_ROLE. '_' . $FW_RUBRIQUE. '_' . $FW_FONCTION. '.php");
			break;';
/* FIN SECTION DEUXIEME NIVEAU FRAGMENT 2 */

/* DEBUT SECTION TROISIEME NIVEAU FRAGMENT 1 */							
							$FONCTION_FILE = '<?php
	$page_name="";
	$soustitre=_T("libSoustitre");

	$description = _T("libDescription");

	$templateFile = "' . $FW_ROLE. '_' . $FW_RUBRIQUE. '_' . $FW_FONCTION. '.html";
	$tmpVars = array("/{CONTENT}/i");
	$replace = array($content);	
	$dynamic_vars = array();
	
?>';
/* FIN SECTION TROISIEME NIVEAU FRAGMENT 1 */		
							$file = "../sections/function_" . $FW_ROLE. "_" . $FW_RUBRIQUE. "_" . $FW_FONCTION. ".php";
							if (!file_exists($file)) {
								$fp = fopen($file, "w");
								fwrite($fp, $FONCTION_FILE);
								fclose($fp);
								chmod("../sections/function_" . $FW_ROLE. "_" . $FW_RUBRIQUE. "_" . $FW_FONCTION. ".php", 0777);
				
							}
							
							$file = "../templates/" .$FW_ROLE. "_" . $FW_RUBRIQUE. "_" . $FW_FONCTION. ".html";
							if (!file_exists($file)) {
								$fp = fopen($file, "w");
								fwrite($fp, "{CONTENT}");
								fclose($fp);
								chmod("../templates/" .$FW_ROLE. "_" . $FW_RUBRIQUE. "_" . $FW_FONCTION. ".html", 0777);
				
							}
						}
						/* fin foreach FONCTIONS */
						echo "</ul>";
					}
					/* fin  FONCTIONS */
					
/* DEBUT SECTION DEUXIEME NIVEAU FRAGMENT 2 */							
					$RUBRIQUE_FILE .= '
	}
	
	$page = new Page($templateDir, $templateFile,  $_SESSION["locale"]);
	$page->setVars($dynamic_vars);
	
	$page->replace_tags($tmpVars, $replace);
	$html = $page->output();
	
?>';
/* FIN SECTION DEUXIEME NIVEAU FRAGMENT 2 */	
					$fp = fopen("../sections/" . $FW_ROLE . "_" . $FW_RUBRIQUE . ".php", "w");
					fwrite($fp, $RUBRIQUE_FILE);
					fclose($fp);
					chmod("../sections/" . $FW_ROLE . "_" . $FW_RUBRIQUE . ".php", 0777);
				}
				/* fin foreach RUBRIQUES */
				echo "</ul>";
			}
			/* fin RUBRIQUES */
/* DEBUT MENU ITEMS FRAGMENT 3 */				
	$MENU_ITEMS = rtrim($MENU_ITEMS, ",");
	$MENU_ITEMS .= '
					),';
	
/* FIN MENU ITEMS FRAGMENT 3 */

/* DEBUT SECTION PREMIER NIVEAU FRAGMENT 3 */	
			$ROLE_FILE .= '
	}
	if(isset($_GET["popin"])){echo $page->output();exit;}else{$html = $page->output();}	
	
	$templateDir = "templates/";
	$templateFile = "main_' . $FW_ROLE . '.html"; 
	$tmpVars = array("/{CONTENT}/i", "/{MENU_PRINCIPAL}/i", "/{TITRE}/i", "/{SOUSTITRE}/i", "/{INFOS_CONNEXION}/i", "/{REDIRECT}/i","/{META_TITLE}/i", "/{META_KEYWORDS}/i", "/{META_DESCRIPTION}/i", "/{BROADCAST}/i");
	$replace = array($html, $menu_principal, $titre, $soustitre, infos_connexion(), $_SERVER["REQUEST_URI"], $meta_title, $meta_keywords, $meta_description, $BROADCAST);
	$page = new Page($templateDir, $templateFile, $_SESSION["locale"]);
	$page->replace_tags($tmpVars, $replace);
	echo $page->output();
?>';
/* FIN SECTION PREMIER NIVEAU FRAGMENT 3 */					
				$fp = fopen("../sections/" . $FW_ROLE . ".php", "w");
				fwrite($fp, $ROLE_FILE);
				fclose($fp);
				chmod("../sections/" . $FW_ROLE . ".php", 0777);
				
			echo "</ul>";
		}
		/* fin foreach ROLES */
		/* Génération du menu principal automatique */
		$MENU_ITEMS = rtrim($MENU_ITEMS, ",");
		$MENU = '<?php

 if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
 
	$menu = array (
					' . $MENU_ITEMS .'
	);
			
	
	
	if(isset($_SESSION["user"]["groupe"]))
	{
		
		
		$menu = $menu[$_SESSION["user"]["groupe"]];
		if(count($menu) > 0)
		{
			$menu_principal = "<nav id=\"menu\"><ul class=\"menu\">";
			foreach ($menu as $item)
			{
				$active = $item["code"] == $_SESSION["contexte"]["rubrique"] ? "on" : "off";
				$class = $item["class"] . " " . $active;
				$menu_principal .= "<li class=\"menu " . $item["code"] . " " . $class . "\"><a href=\"index.php?rubrique=" . $item["code"] . "\" class=\"button\"><span class=\"lib\">" . $item["libelle"] . "</span></a>";
				if(count($item["sousmenu"]) > 0)
				{
					$menu_principal .= "<ul class=\"sousmenu\">";
					foreach ($item["sousmenu"] as $ssitem)
					{
						$ssactive = $ssitem["code"] == $_SESSION["contexte"][$_SESSION["contexte"]["rubrique"]]["page"] ? "on" : "off";
						$class = $ssitem["class"] . " " . $ssactive;
						$menu_principal .= "<li class=\"" . $ssitem["code"] . " " . $class . "\"><a href=\"index.php?page=" . $ssitem["code"] . "\" class=\"button\"><span class=\"lib\">" . $ssitem["libelle"] . "</span></a></li>";
						
					}
				
					$menu_principal .= "</ul>";
				}
				$menu_principal .= "</li>";
			}
			$menu_principal .= "</ul></nav>";
		}
	}
?>';

		$fp = fopen("../sections/menu_principal.php", "w");
		fwrite($fp, $MENU);
		fclose($fp);
		chmod("../sections/menu_principal.php" . $FW_ROLE . ".php", 0777);
	}
	/* fin ROLES */
?>
	</body>
</html>