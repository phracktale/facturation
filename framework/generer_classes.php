<?php
define(_PROFIL_ACCESS, "FRAME-GLOBE");
include('../includes/config.php');
include('../includes/connection.php');
include('utils.php');

$dir = '../classes/db/';
$include_path = "classes/db/";

$query = "show tables";
$result = $conn->query($query);
if($result)
{
	$rows = $result->fetchAll(PDO::FETCH_ASSOC);
	foreach($rows as $row)
	{
		$table = $row['Tables_in_'.$dbName];
		
    	
    	$query = "SHOW fields FROM ".$table;

		$resultFields = $conn->query($query);
		if($resultFields)
		{
			$rowsFields = $resultFields->fetchAll(PDO::FETCH_ASSOC);
			$champs = null;
			$fields = "";
			foreach($rowsFields as $rowField)
			{
				$champs[] = $rowField['Field'];
			}
			
			unset($resultFields);
		}
		$sTable =explode('_', $table);
		if(strtoupper(substr($sTable[0], 0,1)) == "T")
		{
			$page = '<?php'.chr(13);
			$page .= '    /*'.chr(13);
			$page .= '     *  Classe DB '.chr(13);
			$page .= '     *  '.$table.chr(13);
			$page .= '     *  @author: Thierry Cazalet'.chr(13);
			$page .= '     *	Généré automatiquement par frame-globe v0.1'.chr(13);
			$page .= '     *	le '.now().chr(13);
			$page .= '     *	'.chr(13);
			$page .= '     *	Ne jamais éditer ce fichier manuellement '.chr(13);
			$page .= '     */'.chr(13);
			$page .= '	class ' . $table . ' {'.chr(13);
			foreach($champs as $champ)
			{
				$page .= '   	Public $' . $champ . ';'.chr(13);
				$fields .= $sTable[0].'.'.$champ.', ';
			}
			$fields = trim($fields,", ");
			$page .= '   	Private $db;'.chr(13).chr(13);
			$page .= '   	function __construct($value)'.chr(13);
			$page .= '   	{'.chr(13);
			$page .= '     		$this->db = $value;'.chr(13);
			$page .= '   	}'.chr(13).chr(13);
			
			$page .= '   	public function selectByID($value)'.chr(13);
	    	$page .= '   	{'.chr(13);
	    	$page .= '      	$query = "select ' . $fields . ' from ' . $table . ' as ' . $sTable[0] . ' where ' . $sTable[0].'.'. $sTable[0] . '_codeinterne_i=" . $value;'.chr(13);
	    	$page .= '      	$result = $this->db->query($query);'.chr(13);
	      	$page .= '      	if($result)'.chr(13);
			$page .= '      	{'.chr(13);
			$page .= '       	  $row = $result->fetch(PDO::FETCH_OBJ);'.chr(13);
			$page .= '       	  unset($result);'.chr(13);
			$page .= '       	  return $row;'.chr(13);
			$page .= '      	}'.chr(13);
			$page .= '      	else'.chr(13);
			$page .= '      	{'.chr(13);
			$page .= '        	 return false;'.chr(13);
			$page .= '      	}'.chr(13);
	    	$page .= '   	}'.chr(13);
			
			$page .= '   	public function selectAll($orderby = null)'.chr(13);
	    	$page .= '   	{'.chr(13);
	    	$page .= '      	$query = "select ' . $fields . ' from ' . $table . ' as ' . $sTable[0];
			if($orderby!=""){$page .= ' order by ' . $orderby;}
			$page .= ';";'.chr(13);
	    	$page .= '     	 $result = $this->db->query($query);'.chr(13);
	      	$page .= '      	if($result)'.chr(13);
			$page .= '      	{'.chr(13);
			$page .= '       	  $row = $result->fetchAll(PDO::FETCH_OBJ);'.chr(13);
			$page .= '        	 unset($result);'.chr(13);
			$page .= '       	  return $row;'.chr(13);
			$page .= '     	 	}'.chr(13);
			$page .= '      	else'.chr(13);
			$page .= '      	{'.chr(13);
			$page .= '      	   return false;'.chr(13);
			$page .= '      	}'.chr(13);
	    	$page .= '   	}'.chr(13).chr(13);
			
			$page .= '   	function getFields()' . chr(13);
			$page .= '   	{'.chr(13);
			$page .= '   		$query = "SHOW fields FROM ' . $table . ';";' . chr(13);
			$page .= '   		$result = $this->db->query($query);' . chr(13);
			$page .= '   		if($result)'.chr(13);
			$page .= '   		{'.chr(13);
			$page .= '   		  $row = $result->fetchAll(PDO::FETCH_OBJ);' . chr(13);
			$page .= '   			 unset($result);'.chr(13);
			$page .= '   		  return $row;'.chr(13);
			$page .= '   			}'.chr(13);
			$page .= '   		else'.chr(13);
			$page .= '   		{'.chr(13);
			$page .= '   		   return false;'.chr(13);
			$page .= '   		}'.chr(13);
			$page .= '   	}'.chr(13).chr(13);
			$page .= '		public function __call($query, $datas) '.chr(13);
	  		$page .= '		{'.chr(13);
			$page .= '			include(_VHOST_PATH . "' . $include_path . $table . '.select.php");'.chr(13);
			$page .= '			include(_VHOST_PATH . "' . $include_path . $table . '.update.php");'.chr(13);
			$page .= '			include(_VHOST_PATH . "' . $include_path . $table . '.insert.php");'.chr(13);
			$page .= '			include(_VHOST_PATH . "' . $include_path . $table . '.delete.php");'.chr(13);
			
			$page .= '			if(isset($$query))'.chr(13);
			$page .= '			{'.chr(13);
			$page .= '				$sth = $this->db->prepare($$query);'.chr(13);
			$page .= '				$sth->execute($datas);'.chr(13);
			$page .= '				if(isset($_GET[debug])){print_r($sth->errorInfo());}'.chr(13);
			$page .= '				$isInsert = strpos(strtolower($$query), "insert");'.chr(13);
			$page .= '				$isSelect = strpos(strtolower($$query), "select");'.chr(13);
			$page .= '				if($isSelect !== false){$result = $sth->fetchAll(PDO::FETCH_OBJ);}'.chr(13);
			$page .= '				elseif($isInsert !== false){$result =  $this->db->lastInsertId() ;}'.chr(13);
			$page .= '				else{$result == true;}'.chr(13);
			$page .= '				return $result;'.chr(13);
			$page .= '			}'.chr(13);
	        $page .= '			else'.chr(13);
	        $page .= '			{'.chr(13);
	        $page .= '    			throw new queryException("La requèªte  $m est indéfinie");'.chr(13);
	        $page .= '			}'.chr(13);
	  		$page .= '		}'.chr(13);
			$page .= '	}'.chr(13);
			$page .= '?>';
			
	    	$fileName = "class.".$table.".php";
			
			if (file_exists($dir.$table . '.select.php')) 
			{
			}
			else
			{	
				create_file($dir,  $table . '.select.php', $query_file, "");
				chmod($dir.$table . '.select.php', 0777);
			}
			
			if (file_exists($dir.$table . '.update.php')) 
			{
			}
			else
			{	
				create_file($dir,  $table . '.update.php', $query_file, "");
				chmod($dir.$table . '.update.php', 0777);
			}
			if (file_exists($dir.$table . '.insert.php')) 
			{
			}
			else
			{	
				create_file($dir,  $table . '.insert.php', $query_file, "");
				chmod($dir.$table . '.insert.php', 0777);
			}
			
			if (file_exists($dir.$table . '.delete.php')) 
			{
			}
			else
			{	
				create_file($dir,  $table . '.delete.php', $query_file, "");
				chmod($dir.$table . '.delete.php', 0777);
			}
			
	    	if(create_file($dir, $fileName, $page, $message))
	    	{
	    		chmod($dir.$fileName, 0777);
				echo "<br/><small>La classe ".$table." a été sauvée dans le fichier ($fileName)</small>";
	    	}
			else
			{
				echo "<br/><small>La classe ".$table." n'est pas sauvée ($fileName)</small>";
			}
		}
	}
	
	unset($result);
}
?>