<?php
function now()
{
   	$now = getdate(); 
	$today = sprintf("%04d-%02d-%02d %02d:%02d:%02d", $now['year'], $now['mon'], $now['mday'], $now['hours'], $now['minutes'], $now['seconds']);
	return $today;	
}

function create_file($dir, $fileName, $content, $message)
{
	echo $dir.$fileName."<br/>";
	touch($dir.$fileName);
    if (!$handle = fopen($dir.$fileName, 'w')) 
	{
        return false;
        exit;
    }

   	if (fwrite($handle, $content) === FALSE) 
   	{
       		return false;
       		exit;
    }
    
    fclose($handle);
    return true;
}

function menu()
{

	$menu = '<ul class="menuBackend">' .
			'<li><a href="?choix=schema">Base de données</a></li>' .
			'<li><a href="?choix=profils">Profils</a></li>' .
			'<li><a href="?choix=sections">Sections</a></li>' .
			'<li><a href="?choix=fonctions">Fonctions</a></li>' .
			'<li><a href="?choix=modules">Modules</a></li>' .
			'</ul><br class="nettoyeur">';
	return $menu;
}

function array_to_json_string($arraydata) 
{
	$output = "";
	$output .= "{";
	foreach($arraydata as $key=>$val){
		if (is_array($val)) {
			$output .= "\"".$key."\" : [{";
			foreach($val as $subkey=>$subval){
				$output .= "\"".$subkey."\" : \"".$subval."\",";
			}
			$output .= "}],";
		} else {
			$output .= "\"".$key."\" : \"".$val."\",";
		}
	}
	$output .= "}";
	return $output;
}

function init_modules($modulesDir)
{
	$Sys = SystemCore::GetInstance(); // On r�cup�re l'instance unique du Systeme.
	$files = scandir($modulesDir);
	$_SESSION['modules'] ="";			
	/* Ceci est la fa�on correcte de traverser un dossier. */
	$modules .= '<ul>';
	foreach ($files as $file)
	{
		if((false !== is_file($modulesDir.$file))&&(false!==strpos($file, 'class')))
		{
			try 
			{
				include_once($modulesDir.$file);
				$eClass = strlen($file)-17;
				$dClass = strpos($file, "module")+7;
				$classname = substr($file, $dClass, $eClass);
				if(substr($file, 0, 12) == "class.module")
				{
					try
					{
						$$classname = new $classname;
						$_SESSION['modules'][$classname][libelle] = $classname;
						$_SESSION['modules'][$classname][version] = $$classname->getVersion();
						$modules .= '<li>'.$classname.' version '.$$classname->getVersion(); 
						if ( $Sys->isActive(strtolower($classname)) ) 
						{
							$_SESSION['modules'][$classname][actif] = true;
						} 
						$modules .= '</li>';
					}
					catch ( Exception $e ) 
					{
								
					}	
				}
			} 
			catch ( Exception $e ) 
			{
			
			}	
		}	
	}
}

function listeModules()
{
	$modules = '<select  name="nom_composant" id="nom_composant"">'.chr(13);
	foreach($_SESSION[modules] as $module)
	{
		$modules .= "<option>".$module[libelle]."</option>".chr(13);	
	}
	$modules .= '</select>'.chr(13);
	return $modules;
}
?>