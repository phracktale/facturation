<?php

define("_PROFIL_ACCESS", "HUBWIN");
include ('../includes/init.php');

//Initialisation de la liste
$list = array();



//Construction de la requete
$strQuery = "SELECT T10_codepostal_va, T10_ville_va  FROM  t10_communes WHERE ";
if (isset($_POST["T10_codepostal_va"]))
{
    $strQuery .= "T10_codepostal_va LIKE :codePostal ";
}
else
{
    $strQuery .= "T10_ville_va LIKE :ville ";
}

//Limite
if (isset($_POST["maxRows"]))
{
    $strQuery .= "LIMIT 0, :maxRows";
}
$query = $conn->prepare($strQuery);
if (isset($_POST["T10_codepostal_va"]))
{
    $value = $_POST["T10_codepostal_va"]."%";
    $query->bindParam(":codePostal", $value, PDO::PARAM_STR);
}
else
{
    $value = $_POST["T10_ville_va"]."%";
    $query->bindParam(":ville", $value, PDO::PARAM_STR);
}
//Limite
if (isset($_POST["maxRows"]))
{
    $valueRows = intval($_POST["maxRows"]);
    $query->bindParam(":maxRows", $valueRows, PDO::PARAM_INT);
}

$query->execute();

$list = $query->fetchAll(PDO::FETCH_CLASS, "AutoCompletionCPVille");;

echo json_encode($list);


class AutoCompletionCPVille {
	public $T10_ville_va;
	public $T10_codepostal_va;

}
?>