<?php
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	
	define("_PROFIL_ACCESS", "HUBWIN");
	include ('../includes/init.php');
	$communes = $T10->getCommunes(mb_strtoupper (htmlentities($_GET['term'])) ."%");
	if(count($communes)>0)
	{
		foreach ($communes as $commune)
		{
			$_C[] = $commune->T10_ville_va;
		}
		echo json_encode(array_values($_C));
	}
	else
	{
		echo "{Pas de résultat}";
	}

?>