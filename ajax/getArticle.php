<?php
	session_start();
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	
	define("_PROFIL_ACCESS", "FACTURATION");
	include ('../includes/init.php');
	
	$liste_articles = $T08->getProduct($_SESSION['user']['id_user'], $_GET['id']);
	if(count($liste_articles)>0)
	{
		foreach ($liste_articles as $article)
		{
			$_C[] = array(
							"reference" => $article->T08_reference_va,	
							"libelle" => $article->T08_libelle_va,	
							"puht" => $article->T08_prixht_d,
							"tva" => $article->T09_codeinterne_i,							
			);
		}
		echo json_encode($_C);
	}
	else
	{
		echo "{}";
	}

?>