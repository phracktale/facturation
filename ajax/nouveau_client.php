<?php
	session_start();
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	
	define("_PROFIL_ACCESS", "FACTURATION");
	include ('../includes/init.php');
	
	if($_GET['func'] == "ok")
	{
		echo "Client ajouté";
		exit;
	}
	
	$form = new Formulaire($conn);
	$form->load("../formulaires/clients");
	$form->setRedirection("nouveau_client.php?func=ok");
	$form->setStaticInput('T01_codeinterne_i', $_SESSION['user']['id_user']);
	
	if(isset($_POST['submit']))
	{
		$retour = $form->save($_GET['id']);
		$error = $retour['error'];
		
		// test erreur => envoi email de confirmation
	}
	
	$client = $form->get($_GET['id']);
	
	$tmpVars = array();
	$replace = array();
	
	$templateVars = $form->getTemplateFormVars($client, $error, "form");
	$tmpVars = $templateVars['tmpVars'];
	$replace = $templateVars['replace'];
	
	$templateDir = "../templates/";
	$templateFile = "entreprise_client_client_nouveau.html";
	$page = new Page($templateDir, $templateFile, $_SESSION["locale"]);
	$page->replace_tags($tmpVars, $replace);
	$html =  $page->output();
	$templateDir = "./";
	$templateFile = "popin.html"; 
	$tmpVars = array("/{CONTENT}/i"); 
	$replace = array($html); 
	$page = new Page($templateDir, 
	$templateFile, $_SESSION["locale"]);
	$page->replace_tags($tmpVars, $replace);
	echo $page->output();
	
?>