<?php
	session_start();
	include("../includes/config.php");
	if($_GET['method']  == "latLng")
	{
		$adresse = urlencode($_GET['lat'] . ", " .  $_GET['lng'] );
		$_SESSION['user']['adresse']['latitude'] = $_GET['lat'] ;
		$_SESSION['user']['adresse']['longitude'] = $_GET['lng'];
	}
	else if($_GET['method']  == "address")
	{
		$adresse = urlencode($_GET['address'] . " ,France");	
	}
	
	//$geocode_url = signUrl("http://maps.googleapis.com/maps/api/geocode/json?address=" .  $adresse . "&sensor=true&language=fr" . "&client=" . _GEOCODE_CLIENT, _GEOCODE_SIGNATURE);
	$geocode_url = "http://maps.googleapis.com/maps/api/geocode/json?address=" .  urlencode ( $adresse ). "&sensor=true&language=fr";
	if($reponse = file_get_contents ( $geocode_url ))
	{
		echo $reponse;
	}
	else
	{
		var_dump($http_response_header);
	}
	
	// Encode a string to URL-safe base64
	function encodeBase64UrlSafe($value) {
		return str_replace(array('+', '/'), array('-', '_'),
			base64_encode($value));
	}

	// Decode a string from URL-safe base64
	function decodeBase64UrlSafe($value) {
		return base64_decode(str_replace(array('-', '_'), array('+', '/'),
			$value));
	}

	// Sign a URL with a given crypto key
	// Note that this URL must be properly URL-encoded
	function signUrl($myUrlToSign, $privateKey) {
		// parse the url
		$url = parse_url($myUrlToSign);

		$urlPartToSign = $url['path'] . "?" . $url['query'];

		// Decode the private key into its binary format
		$decodedKey = decodeBase64UrlSafe($privateKey);

		// Create a signature using the private key and the URL-encoded
		// string using HMAC SHA1. This signature will be binary.
		$signature = hash_hmac("sha1",$urlPartToSign, $decodedKey, true);

		$encodedSignature = encodeBase64UrlSafe($signature);

		return $myUrlToSign."&signature=".$encodedSignature;
	}
?>