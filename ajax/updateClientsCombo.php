<?php
	session_start();
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	
	define("_PROFIL_ACCESS", "FACTURATION");
	include ('../includes/init.php');
	
	
	$clients = $T03->getCustomerByUser($_SESSION['user']['id_user']);	
	if (count($clients) > 0)
	{
		$liste_clients = "<option></option>";
		foreach($clients as $client)
		{
			if($facture[0]->T03_codeinterne_i == $client->T03_codeinterne_i)
			{
				$isSelected = 'selected="selected"';
			}
			else
			{
				$isSelected = "";
			}
			
			
			$client_libelle = $client->T03_contact_nom_va . " " . $client->T03_contact_prenom_va;
			
			if($client->T03_raison_sociale_va != "")
			{
				$client_libelle = $client->T03_raison_sociale_va;
				if($client->T03_contact_nom_va != "")
				{
					$client_libelle .= " (" . $client_libelle . ")";
				}
		
			}
			$liste_clients .= '<option value="' . $client->T03_codeinterne_i . '" ' . $isSelected . '>' . $client_libelle . '</option>';
		}
	}
	
	echo $liste_clients;
?>