$(document).ready(function(){
		var _do_google_connect = function() {
			var google_auth = $('#social_connect_google_auth');
			var redirect_uri = google_auth.find('input[type=hidden][name=redirect_uri]').val();

			window.open(redirect_uri,'','scrollbars=no,menubar=no,height=400,width=800,resizable=yes,toolbar=no,status=no');
		};

		var _do_facebook_connect = function() {
			var facebook_auth = $('#social_connect_facebook_auth');
			var client_id = facebook_auth.find('input[type=hidden][name=client_id]').val();
			var redirect_uri = facebook_auth.find('input[type=hidden][name=redirect_uri]').val();

			if(client_id == "") {
				alert("Social Connect plugin has not been configured for this provider")
			} else {
				window.open('https://graph.facebook.com/oauth/authorize?client_id=' + client_id + '&redirect_uri=' + redirect_uri + '&scope=email,user_location,user_birthday',
				'','scrollbars=no,menubar=no,height=500,width=900,resizable=yes,toolbar=no,status=no');
			}
		};

		// Close dialog if open and user clicks anywhere outside of it
		function overlay_click_close() {
			if (closedialog) {
				_social_connect_already_connected_form.dialog('close');
			}
			closedialog = 1;
		}

		$(".social_connect_login_facebook").click(function() {
			_do_facebook_connect();
		});

		$(".social_connect_login_continue_facebook").click(function() {
			_do_facebook_connect();
		});

		

		$(".social_connect_login_google").click(function() {
			_do_google_connect();
		});

		$(".social_connect_login_continue_google").click(function() {
			_do_google_connect();
		});
});

window.wp_social_connect = function(config) {
	$('#loginform').unbind('submit.simplemodal-login');

	var form_id = '#loginform';

	if(!$('#loginform').length) {
		// if register form exists, just use that
		if($('#registerform').length) {
			form_id = '#registerform';
		} else {
			// create the login form
			var login_uri = $("#social_connect_login_form_uri").val();
			$('body').append("<form id='loginform' method='post' action='" + login_uri + "'></form>");
			$('#loginform').append("<input type='hidden' id='redirect_to' name='redirect_to' value='" + window.location.href + "'>");
		}
	}

	$.each(config, function(key, value) { 
		$("#" + key).remove();
		$(form_id).append("<input type='hidden' id='" + key + "' name='" + key + "' value='" + value + "'>");
	});  

	if($("#simplemodal-login-form").length) {
		var current_url = window.location.href;
		$("#redirect_to").remove();
		$(form_id).append("<input type='hidden' id='redirect_to' name='redirect_to' value='" + current_url + "'>");
	}

	$(form_id).submit();
}
