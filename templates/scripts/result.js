{
   "results" : [
      {
         "address_components" : [
            {
               "long_name" : "NA-7513",
               "short_name" : "NA-7513",
               "types" : [ "route" ]
            },
            {
               "long_name" : "Navarra",
               "short_name" : "NA",
               "types" : [ "administrative_area_level_2", "political" ]
            },
            {
               "long_name" : "Communauté forale de Navarre",
               "short_name" : "Communauté forale de Navarre",
               "types" : [ "administrative_area_level_1", "political" ]
            },
            {
               "long_name" : "Espagne",
               "short_name" : "ES",
               "types" : [ "country", "political" ]
            },
            {
               "long_name" : "31891",
               "short_name" : "31891",
               "types" : [ "postal_code" ]
            }
         ],
         "formatted_address" : "NA-7513, 31891, Navarra, Espagne",
         "geometry" : {
            "bounds" : {
               "northeast" : {
                  "lat" : 43.01264970,
                  "lng" : -1.99972170
               },
               "southwest" : {
                  "lat" : 43.01091130,
                  "lng" : -2.00265040
               }
            },
            "location" : {
               "lat" : 43.01202790,
               "lng" : -2.002090
            },
            "location_type" : "APPROXIMATE",
            "viewport" : {
               "northeast" : {
                  "lat" : 43.01312948029150,
                  "lng" : -1.99972170
               },
               "southwest" : {
                  "lat" : 43.01043151970850,
                  "lng" : -2.00265040
               }
            }
         },
         "types" : [ "route" ]
      }
   ],
   "status" : "OK"
}