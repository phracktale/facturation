var lines = {};

var line = "";
var x = 0;

function calculette_ttc(pttc, taux_tva)
{
	var pht = pttc / ( 1 + taux_tva*1);
	pht = Math.round(pht*10000, 4)/10000;
	return pht;
}

function get_line(objLine)
{
	lineSel = $(objLine).parent().parent().parent();
	line_id = $(lineSel).attr('id');
	line_id = line_id.split('_');
	return line_id[1];
}

function getLineItem(item, line_id)
{
	return $('*[name=' + item + '_' + line_id + ']').val()	;
}

function setLineItem(item, line_id, val)
{
	$('*[name=' + item + '_' + line_id + ']').val(val);
}

function new_line(event, type_line, type_article){
	n_line = $('*[name=n_line]').val();
	n_line++;
	$('*[name=n_line]').val(n_line);
	
	switch(type_line)
	{
		case "line":
			var quantite = 1;
			var libelle = "test";
			var tva = "1";
			var puht = 0;
			var pht = 0;
			
			if($('select[name=produit]').val() != "" || $('select[name=service]').val() != "" )
			{
				$.get('ajax/getArticle.php', {'id': $('select[name=' + type_article + ']').val()}, function(response){
					if(response != "{}")
					{
						response = $.parseJSON(response)
						response = response[0];
						libelle = response.libelle;
						puht = response.puht;
						pht = response.puht;
						$.when(setLine(quantite, libelle, puht, pht, tva) ).then(function(){setTotal()});
					}
					else
					{
						libelle = "";
						puht = 0;
						pht = 0;
						quantite = 1;
						$.when(setLine(quantite, libelle, puht, pht, tva) ).then(function(){setTotal()});
					}
				});
			}
			else
			{
				setLine(quantite, libelle, puht, pht, tva)
			}
			break;
			
		case "titre":
			titre = "";
			line = '<div rel="titre" id="line_' + n_line + '" class="line line_' + n_line + '"><ul class="table w100"><li class="col pa1"><input type="text" class="titre w100" name="titre_' + n_line + '" value="' + titre + '"/></li><li class="col pa1 line_suppr"><a class="supprime" href="#" title="supprimer ligne"><img src="templates/images/icon_delete.png" alt="Supprimer ligne"  width="24" height="24"/></a></li></ul></div>';
			$('#facturation').append(line)
			lines[n_line] = {'type': 'titre', 'libelle': titre};
			setActions();
			break;
			
		case "commentaire":
			commentaire = ""
			line = '<div rel="commentaire" id="line_' + n_line + '" class="line line_' + n_line + '"><ul class="table w100"><li class="col pa1"><textarea type="text" class="commentaire w100" name="commentaire_' + n_line + '">' + commentaire + '</textarea></li><li class="col pa1 line_suppr"><a class="supprime" href="#" title="supprimer ligne"><img src="templates/images/icon_delete.png" alt="Supprimer ligne"  width="24" height="24"/></a></li></ul></div>';
			$('#facturation').append(line)
			lines[n_line] = {'type': 'commentaire', 'libelle': commentaire};
			setActions();
			break;
	}
}
function setLine(quantite, libelle, puht, pht, tva)
{
	line = '<div rel="ligne" id="line_' + n_line + '" class="line line_' + n_line + '"><a href="#" class="grip"></a><ul class="table"><li class="col pa1 w500p"><textarea  class="libelle w100" name="libelle_' + n_line + '">' + libelle + '</textarea></li><li class="col pa1 w50p"><input type="text" class="quantite w100 txtright" name="quantite_' + n_line + '" value="' + quantite + '"/></li><li class="col pa1 w100p"><select class="tva w100" name="tva_' + n_line + '" ><option value="0.2">20%</option><option value="0.196">19.6%</option><option value="0.085">8.5%</option><option value="0.07">7%</option><option value="0.055">5.5%</option><option value="0.021">2.1%</option><option value="0">0%</option></select></li><li class="col pa1 w100p"><div class="mini-tool"><a href="#" class="calculette_ttc">TTC</a></div><input type="text" class="puht w100 txtright" name="puht_' + n_line + '" value="' + puht + '"/></li><li class="col pa1 w100p"><input type="text" class="pht w100 txtright" name="pht_' + n_line + '" value="' + pht + '" readonly/></li><li class="col pa1 line_suppr"><a class="supprime" href="#" title="supprimer ligne"><img src="templates/images/icon_delete.png" alt="Supprimer ligne"  width="24" height="24"/></a></li></ul></div>';
	$('#facturation').append(line)
	lines[n_line] = {type: 'ligne', quantite: quantite, libelle: libelle, tva: tva, puht:puht, pht:pht}
	
	$('textarea[name=libelle_' + n_line +']').autosize();
	
	setActions();
}

function getTotalHT()
{
	var totalHT = 0
	$('.pht').each(function(){
		totalHT += $(this).val()*1;
	});
	return totalHT
}



function getTotalTVA()
{
	var totalTVA = 0
	$('div[rel=ligne]').each(function(){
		line_id = $(this).attr('id');
		line_id = line_id.split('_');
		line_id = line_id[1];
		quantite = getLineItem('quantite', line_id);
		tva = getLineItem('tva', line_id);
		pht = getLineItem('puht', line_id) * quantite;
		totalTVA += tva * pht;
	});
	
	return totalTVA
}

function getTotalTTC()
{
	var totalTVA = 0
	var totalTTC = 0;
	$('div[rel=ligne]').each(function(){
		line_id = $(this).attr('id');
		line_id = line_id.split('_');
		line_id = line_id[1];
		quantite = getLineItem('quantite', line_id);
		tva = getLineItem('tva', line_id);
		pht = getLineItem('puht', line_id) * quantite;
		totalTVA += tva*pht;
		totalTTC += pht + totalTVA;
	});
	
	return totalTTC
}

function line_update()
{
	line_id = 1;
	$('.line').each(function(){
		$(this).attr('id',  "line_" + line_id);
		$(this).find('.titre').attr('name',  "titre_" + line_id);
		$(this).find('.libelle').attr('name',  "libelle_" + line_id);
		$(this).find('.quantite').attr('name',  "quantite_" + line_id);
		$(this).find('.tva').attr('name',  "tva_" + line_id);
		$(this).find('.puht').attr('name',  "puht_" + line_id);
		$(this).find('.pht').attr('pht',  "tva_" + line_id);
		$(this).find('.commentaire').attr('name_',  "commentaire" + line_id);
		line_id++;
	});
}

function setLineObj()
{
	$('.line').each(function(){
		line_id = $(this).attr('id');
		line_id = line_id.split('_');
		line_id = line_id[1];
		
		
		switch($(this).attr('rel'))
		{
			case "ligne":
				libelle = getLineItem('libelle', line_id);
				quantite = getLineItem('quantite', line_id);
				tva = getLineItem('tva', line_id);
				puht = getLineItem('puht', line_id);
				pht = getLineItem('puht', line_id) * quantite;
				
				
				setLineItem('pht', line_id, pht);
				lines[line_id] = {'type': 'ligne', 'quantite': quantite, 'libelle': libelle, 'tva': tva, 'puht': puht, 'pht': pht};
				break;
			case "titre":
				libelle = getLineItem('titre', line_id);
				lines[line_id] = {'type': 'titre', 'libelle': libelle};
				break;
				
			case "commentaire":
				libelle = getLineItem('commentaire', line_id);
				lines[line_id] = {'type': 'commentaire', 'libelle': libelle};
				break;
		}
	});
	setTotal();
}

function setTotal()
{
	piece_sousTotalHT = parseFloat(Math.round(getTotalHT() * 100) / 100).toFixed(2);
	piece_remise = $('input[name=remise_globale]').val() * 1;
	piece_tva = parseFloat(Math.round((getTotalTVA()  - piece_remise * getTotalTVA() / 100)* 100) / 100).toFixed(2);
	piece_totalHT = parseFloat(Math.round((getTotalHT() - piece_remise * getTotalHT() / 100) * 100) / 100).toFixed(2)
	piece_totalTTC = parseFloat(Math.round(((getTotalHT() - piece_remise * getTotalHT() / 100) + (getTotalTVA()  - piece_remise * getTotalTVA() / 100)) * 100) / 100).toFixed(2);
	
	
	
	$('#sousTotalHT').text(piece_sousTotalHT);
	$('#totalHT').text(piece_totalHT);
	$('#totalTVA').text(piece_tva);
	$('#totalTTC').text(piece_totalTTC);
}

function setActions()
{
	$('.calculette_ttc').off();
	$('.calculette_ttc').on('click', function(event){
		event.preventDefault();
		line_id = get_line($(this).parent());
		taux_tva = $('select[name=tva_' + line_id + ']').val();

		var pht = prompt("Saisissez Le prix TTC pour le convertir en prix HT :", "");
		
		if (pht != null) {
			pht = calculette_ttc(pht, taux_tva);
			$('#line_' + line_id + ' input.puht').val(pht);
			$('#line_' + line_id + ' input.pht').val(parseFloat($('#line_' + line_id + ' input.puht').val()) * parseFloat($('#line_' + line_id + ' input.quantite').val()));
			setLineObj();
			setTotal();
		}
	});
	
	$('.supprime').off();
	$('.supprime').on('click', function(event){
		lineSel = $(this).parent().parent().parent();
		line_id = $(lineSel).attr('id');
		line_id = line_id.split('_');
		line_id=line_id[1];
		delete lines[line_id];
		$.when($(lineSel).remove() ).then(function(){setTotal()});
		event.preventDefault();
	});
	
	$('.quantite').off();
	$('.quantite').on('keyup', function(){
		line_id = get_line($(this));
		quantite = getLineItem('quantite', line_id);
		libelle = getLineItem('libelle', line_id);
		tva = getLineItem('tva', line_id);
		puht = getLineItem('puht', line_id);
		pht = getLineItem('puht', line_id) * quantite;
		setLineItem('pht', line_id, pht);
		lines[line_id] = {'type': 'ligne', 'quantite': quantite, 'libelle': libelle, 'tva': tva, 'puht': puht, 'pht': pht}
		setTotal();
	});
	
	$('.puht').off();
	$('.puht').on('keyup', function(){
		line_id = get_line($(this));
		quantite = getLineItem('quantite', line_id);
		libelle = getLineItem('libelle', line_id);
		tva = getLineItem('tva', line_id);
		puht = getLineItem('puht', line_id);
		pht = getLineItem('puht', line_id) * quantite;
		setLineItem('pht', line_id, pht);
		lines[line_id] = {'type': 'ligne', 'quantite': quantite, 'libelle': libelle, 'tva': tva, 'puht': puht, 'pht': pht}
		setTotal();
	});
	
	$('.libelle').on('keyup', function(){
		
		line_id = get_line($(this));
		quantite = getLineItem('quantite', line_id);
		libelle = getLineItem('libelle', line_id);
		tva = getLineItem('tva', line_id);
		puht = getLineItem('puht', line_id);
		pht = getLineItem('puht', line_id) * quantite;
		lines[line_id] = {'type': 'ligne', 'quantite': quantite, 'libelle': libelle, 'tva': tva, 'puht': puht, 'pht': pht}
		setTotal();
	});
	
	$('.tva').off();
	$('.tva').on('change', function(){
		line_id = get_line($(this));
		quantite = getLineItem('quantite', line_id);
		libelle = getLineItem('libelle', line_id);
		tva = getLineItem('tva', line_id);
		puht = getLineItem('puht', line_id);
		pht = getLineItem('puht', line_id) * quantite;
		lines[line_id] = {'type': 'ligne', 'quantite': quantite, 'libelle': libelle, 'tva': tva, 'puht': puht, 'pht': pht}
		setTotal();
	});
	
	$('.titre').off();
	$('.titre').on('keyup', function(){
		line_id = get_line($(this));
		lines[line_id] = {'type': 'titre', 'libelle': $(this).val()}
	});
	
	$('.commentaire').off();
	$('.commentaire').on('keyup', function(){
		line_id = get_line($(this));
		lines[line_id] = {'type': 'commentaire', 'libelle': $(this).val()}
	});
}

function sauve_piece(mode_sauvegarde)
{
	// data = JSON.stringify(lines, null, 2);
	code_interne = $('*[name=code_interne]').val();
	intitule = $('*[name=intitule]').val();
	date_echeance = $('*[name=date_echeance]').val();
	numero_piece = $('*[name=numero_piece]').val();
	remise_globale = $('*[name=remise_globale]').val()
	total_ht = parseFloat($('#totalHT').text());
	total_tva = parseFloat($('#totalTVA').text());
	total_ttc = parseFloat($('#totalHT').text()) + parseFloat($('#totalTVA').text());
	remarque = $('*[name=remarque]').val();
	moyenReglement = $('*[name=moyenReglement]').val()
	client_id = $('*[name=client_id]').val();
	
	
	piece = {'entete': {
						'mode_sauvegarde': mode_sauvegarde, 
						'code_interne': code_interne, 
						'numero_piece': numero_piece, 
						'date_echeance': date_echeance,
						'intitule': intitule,
						'moyenReglement': moyenReglement,
						'client_id': client_id,
						'remise_globale': remise_globale,
						'total_ht': total_ht,
						'total_tva': total_tva,
						'total_ttc': total_ttc,
						'remarque': remarque,
						},
				'lines': lines
	}
	
	return piece;
}


$(document).ready(function(){

	$(".fancybox").fancybox({
		beforeLoad : function(){$.get("ajax/rubState.php", {"func": "save"});},
		afterClose : function(){$.get("ajax/rubState.php", {"func": "restore"});},
		autoSize : false,
        beforeLoad : function() {         
            this.width  = parseInt(this.element.data('fancybox-width'));
        }
	});
	
	$(".fancybox-nclient").fancybox({
		beforeLoad : function(){
			$.get("ajax/rubState.php", {"func": "save"});
		},
		afterClose : function(){
			$.get("ajax/rubState.php", {"func": "restore"});
			
			$.get("ajax/updateClientsCombo.php", {}, function(data){$('#clients').html(data);})	;
			
		},
		autoSize : false,
        beforeLoad : function() {         
            this.width  = parseInt(this.element.data('fancybox-width'));
        }
	});
	
	setLineObj();
	$('textarea.libelle').autosize();
	
	
	if($('#broadcast').html() != "")
	{
		$('#broadcast').html('<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 20px; color: #f5be29;"><p><span class="ui-icon ui-icon-info" style="float: left; margin-right: 1em;"></span>' + $('#broadcast').html	() + '</p></div></div>');
	}
	$('input[name=remise_globale]').keyup(function(event){
		setTotal();
	});
	
	
	$( "#facturation" ).sortable({
      placeholder: "ui-state-highlight",
	  update: function( event, ui ) {
		line_update();
		setLineObj();
	  }
    });
    $( "#facturation" ).disableSelection();
	
	/* Factures, devis, avoir */
	$('#save').click(function(event){
		piece = sauve_piece('brouillon');
		console.log(piece);
		$.post( "index.php?page=" + $(this).attr('rel') + "_sauver&popin", piece, function( response ) {
			console.log(response);
			broadcast('Sauvegarde', 'Votre document est sauvegardé');
			$("#valider").prop("disabled", false);
			response = $.parseJSON(response)
			$('input[name=code_interne]').val(response.code_interne);
			
		});
		event.preventDefault()
	});
	
	/* Facture */
	$('#valider').click(function(event){
		piece = sauve_piece('valider');
		if(getTotalHT() == 0)
		{
			broadcast("Attention", "Vous ne pouvez pas sauver la facture si le total est nul");
		}
		else if($('input[name=date_echeance]').val() == "")
		{
			broadcast("Attention", "Vous n'avez pas indiqué de date d'échéance, la facture ne peut pas être validée");
		}
		else
		{
			$.post( "index.php?page=" + $(this).attr('rel') + "_sauver&popin", piece, function( response ) {
				response = $.parseJSON(response)
				$('input[name=numero_piece]').val(response.numero_piece);
				$("#save").prop("disabled", true);
				$("#valider").prop("disabled", true);
				broadcast('Sauvegarde', 'Votre facture est validée');
			});
		}
		event.preventDefault()
	});
	
	/* Devis */
	$('#transferer_facture').click(function(event){
		piece = sauve_piece('transfert_facture');
		piece.entete.devis_parent = $('input[name=code_interne]').val();
		console.log(piece);
		$.post( "index.php?page=devis_sauver&popin", piece, function( response ) {
			console.log(response);
			broadcast('Sauvegarde', 'Votre devis est transféré en tant que facture');
			
			$("#save").prop("disabled", true);
			$("#transferer_facture").prop("disabled", true);
			$('#transferer_acompte_param').prop('disabled', true);
			$("#nouveau_service").prop("disabled", true);
			$("#nouveau_produit").prop("disabled", true);
			$("#new_title").prop("disabled", true);
			$("#new_com").prop("disabled", true);
			
			response = $.parseJSON(response)
		});
		event.preventDefault()
	});
	
	$('#transferer_acompte_param').click(function(event){
		$('#contSave').height($('#contSave').height() + $('#param_acompte').height() + 20);
		$('#param_acompte').show('fast');
		$('#transferer_acompte_param').prop('disabled', true);
		$('.export_piece').prop('disabled', true);
		$("#save").prop("disabled", true);
		$("#transferer_facture").prop("disabled", true);
		$("#nouveau_service").prop("disabled", true);
		$("#nouveau_produit").prop("disabled", true);
		$("#new_title").prop("disabled", true);
		$("#new_com").prop("disabled", true);
		event.preventDefault()
	});

	$('#transferer_acompte').click(function(event){

		tva = parseFloat($('#param_acompte .taux_tva').val()) * parseFloat($('#param_acompte .acompte_montantHT').val());
		$('#param_acompte .acompte_tva').val(tva);


		piece = sauve_piece('transfert_acompte');
		
		piece.entete.devis_parent = $('input[name=acompte_montantHT]').val();
		piece.entete.total_ht = parseFloat($('input[name=acompte_montantHT]').val());
		piece.entete.total_tva = tva;
		piece.entete.total_ttc = piece.entete.total_ht + tva;
		piece.lines = {"1":{type: "ligne", quantite: 1, libelle: "Acompte sur le devis n°" + $('input[name=numero_piece]').val(), tva: tva, puht:piece.entete.total_ht, pht:piece.entete.total_ht}}
		
		console.log(piece);

		$.post( "index.php?page=devis_sauver&popin", piece, function( response ) {
			console.log(response);
			broadcast('Sauvegarde', "Une facture d'acompte a été généré à partir de ce devis");
			$('#param_acompte').hide('fast');
			$('#transferer_acompte_param').prop('disabled', false);
			$('.export_piece').prop('disabled', false);
			$("#save").prop("disabled", false);
			$("#transferer_facture").prop("disabled", false);
			$("#nouveau_service").prop("disabled", false);
			$("#nouveau_produit").prop("disabled", false);
			$("#new_title").prop("disabled", false);
			$("#new_com").prop("disabled", false);
		});
		event.preventDefault()
	});
	
	$('input[name=acompte_montantHT]').keyup(function(event){
		reste = getTotalHT() - parseFloat($('input[name=total_acompte]').val());
		if(parseFloat($(this).val()) > reste )
		{
			$(this).val(reste);
			broadcast('Attention', "Vous ne pouvez pas facturer plus que le solde restant du devis soit " + reste + "€HT");
			$('#transferer_acompte').prop('disabled', true)
			$('#transferer_facture').prop('disabled', false)
			
		}
		if(parseFloat($(this).val()) < reste )
		{
			$('#transferer_acompte').prop('disabled', false)
			$('#transferer_facture').prop('disabled', true)
		}
	});
	
	
	$('#nouveau_service').click(function(event){new_line(event, 'line', 'service');event.preventDefault();});
	$('#nouveau_produit').click(function(event){new_line(event, 'line', 'produit');event.preventDefault()});
	
	$('#new_title').click(function(event){new_line(event, 'titre');event.preventDefault();});
	$('#new_com').click(function(event){new_line(event, 'commentaire');event.preventDefault();});
	
	$('.export_piece').click(function(event){
		doc = $(this).attr('rel').split('|');
		document.location.href = "index.php?page=" + doc[0] + "_export&num_piece=" + doc[1] + "&format=" + doc[2] + "&popin";
	});
	
	
	var language_options = {
					"sProcessing":     "Traitement en cours...",
					"sSearch":         "Rechercher&nbsp;:",
					"sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
					"sInfo":           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
					"sInfoEmpty":      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
					"sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
					"sInfoPostFix":    "",
					"sLoadingRecords": "Chargement en cours...",
					"sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
					"sEmptyTable":     "Aucune donnée disponible dans le tableau",
					"oPaginate": {
						"sFirst":      "Premier",
						"sPrevious":   "Pr&eacute;c&eacute;dent",
						"sNext":       "Suivant",
						"sLast":       "Dernier"
					},
					"oAria": {
						"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
						"sSortDescending": ": activer pour trier la colonne par ordre décroissant"
					}
				}
            
	$('.datatable_pieces').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"iDisplayLength": 50,
		 "aoColumnDefs": [
				 { "asSorting": [ "desc" ], "aTargets": [ 0 ] },
		 		{ "asSorting": [ "desc" ], "aTargets": [ 1 ] },
			  	{ "bSortable": false, "aTargets": [ 6, 7, 8, 9 ] }
			]
		 ,
		"oLanguage": language_options
            
	})
	
	
	
	$('.datatable').dataTable({
		"bJQueryUI": true,
		"sPaginationType": "full_numbers",
		"iDisplayLength": 50,
		"oLanguage": language_options
	})
});

window.broadcast = function(titre, texte) {
    var target = $('.qtip.jgrowl:visible:last');

    $('<div/>').qtip({
        content: {
            text: texte,
            title: {
                text: titre,
                button: true
            }
        },
        position: {
            target: [0,0],
            container: $('#qtip-growl-container')
        },
        show: {
            event: false,
            ready: true,
            effect: function() {
                $(this).stop(0, 1).animate({ height: 'toggle' }, 400, 'swing');
            },
            delay: 0,
            persistent: texte
        },
        hide: {
            event: false,
            effect: function(api) {
                $(this).stop(0, 1).animate({ height: 'toggle' }, 400, 'swing');
            }
        },
        style: {
            width: 250,
            classes: 'jgrowl',
            tip: false
        },
        events: {
            render: function(event, api) {
                if(!api.options.show.texte) {
                    $(this).bind('mouseover mouseout', function(e) {
                        var lifespan = 5000;

                        clearTimeout(api.timer);
                        if (e.type !== 'mouseover') {
                            api.timer = setTimeout(function() { api.hide(e) }, lifespan);
                        }
                    })
                    .triggerHandler('mouseout');
                }
            }
        }
    });
}