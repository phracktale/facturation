{
   "results" : [
      {
         "address_components" : [
            {
               "long_name" : "Bacou",
               "short_name" : "Bacou",
               "types" : [ "route" ]
            },
            {
               "long_name" : "Véraza",
               "short_name" : "Véraza",
               "types" : [ "locality", "political" ]
            },
            {
               "long_name" : "Aude",
               "short_name" : "11",
               "types" : [ "administrative_area_level_2", "political" ]
            },
            {
               "long_name" : "Languedoc-Roussillon",
               "short_name" : "Languedoc-Roussillon",
               "types" : [ "administrative_area_level_1", "political" ]
            },
            {
               "long_name" : "France",
               "short_name" : "FR",
               "types" : [ "country", "political" ]
            },
            {
               "long_name" : "11580",
               "short_name" : "11580",
               "types" : [ "postal_code" ]
            }
         ],
         "formatted_address" : "Bacou, 11580 Véraza, France",
         "geometry" : {
            "bounds" : {
               "northeast" : {
                  "lat" : 43.0125940,
                  "lng" : 2.3137670
               },
               "southwest" : {
                  "lat" : 43.010880,
                  "lng" : 2.31023080
               }
            },
            "location" : {
               "lat" : 43.01181540,
               "lng" : 2.31150560
            },
            "location_type" : "APPROXIMATE",
            "viewport" : {
               "northeast" : {
                  "lat" : 43.01308598029150,
                  "lng" : 2.3137670
               },
               "southwest" : {
                  "lat" : 43.01038801970850,
                  "lng" : 2.31023080
               }
            }
         },
         "types" : [ "route" ]
      }
   ],
   "status" : "OK"
}