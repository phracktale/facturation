<?php
    session_start();
	if (!defined("_PROFIL_ACCESS")){header('location:erreur');}
	
	
	$_SESSION['contexte']['rubrique'] = isset($_REQUEST['rubrique']) ? $_REQUEST['rubrique'] : $_SESSION['contexte']['rubrique'];
	$_SESSION['contexte'][$_SESSION['contexte']['rubrique']]['page'] = isset($_REQUEST['page']) ? $_REQUEST['page'] : $_SESSION['contexte'][$_SESSION['contexte']['rubrique']]['page'];
	
	include ('includes/init.php');

	$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";
	
	switch($action)
	{
		default:
			$module = strtolower($_SESSION['user']['groupe']);
			include("sections/".$module.'.php');
			exit;
			break;
		
		case "erreur":
		case "login":
			$page_id="login";
			
			if(isset($_GET['error']))
			{
				switch ($_GET['error'])
				{
					case "oAuth":
						$message = _T('libErrorOauthMessage');
						break;
				}
			}
			
			$templateFile = "login.html"; 
			$tmpVars = array("/{MESSAGE}/i", "/{FORM_CONNEXION}/i", "/{SOCIAL_CONNECT}/i", "/{RETURN_TO}/i", "/{CLOSE_SCRIPT}/i");
			$replace = array($message, $form_connexion, $social_connect, $return_to, $close_script);
			$page = new Page($mainDir, $templateFile, "");
			$page->replace_tags($tmpVars, $replace);
			$login =  $page->output();
			
			$templateFile = "main_public.html"; 
			$tmpVars = array("/{CONTENT}/i", "/{MENU_PRINCIPAL}/i", "/{TITRE}/i", "/{SOUSTITRE}/i", "/{INFOS_CONNEXION}/i", "/{REDIRECT}/i", "/{UNIVERS}/i", "/{UNIVERS_OFFRES}/i", "/{UNIVERS_DEMANDES}/i", "/{META_TITLE}/i", "/{META_KEYWORDS}/i", "/{META_DESCRIPTION}/i");
			$replace = array($login, $menu_principal, $titre, $soustitre, infos_connexion(), _VHOST_URL . "accueil", $_SESSION['univers']['actif'], $_SESSION['univers']['offres'], $_SESSION['univers']['demandes'], $metas[$page_id]['title'], $metas[$page_id][$page]['keywords'], $metas[$page_id]['description']);
			$page = new Page($templateDir, $templateFile, $_SESSION['locale']);
			$page->replace_tags($tmpVars, $replace);
			echo $page->output();
			break;
			
		case "verifLogin":
			if( isset($_REQUEST["login"]) && isset($_REQUEST["password"]) )
			{
				verifLogin($conn, $_REQUEST['login'], $_REQUEST['password']);
			}
			elseif( isset($_REQUEST["provider"]) )
			{
				$provider = $_REQUEST["provider"];
				
				try{
					// initialize Hybrid_Auth with a given file
					
					$hybridauth = new Hybrid_Auth( _VHOST_PATH . '/classes/hybridauth/hybridauth/config.php' );
					$adapter = $hybridauth->authenticate( $provider );
					$user = $adapter->getUserProfile();
					log_user($user, $provider);
				}
				catch( Exception $e )
				{
					echo "Error: please try again!";
					echo "Original error message: " . $e->getMessage();
				}
				
				
				
				/*
				if(!)
				{
					header('location:editer_profil');
					exit;
				}
				else
				{
					header('location:accueil');
					exit;
				}
				*/
			}    
			
			
			if($_SESSION['maintenance'] == "on" && ($_SESSION['user']['role'] != "ADMIN" || $_SESSION['user']['role'] != "SUPERADMIN" ))
			{
				session_destroy();
			}
			header('location:index.php');
			break;
		
			
		case "maintenance_login":
			if($_SESSION['access'] == "erreur")
			{
				$message = "Problème à l'identification<br/>Veuillez recommencer";
			}
			
			$templateFile = "form_connexion.html"; 
			$tmpVars = array("/{MESSAGE}/i", "/{SOCIAL_CONNECT}/i");
			$replace = array($message, $social_connect);
			$page = new Page($templateDir, $templateFile,  $_SESSION['locale']);
			$page->replace_tags($tmpVars, $replace);
			$form_connexion = $page->output();
			
			$templateFile = "accueil.html"; 
			$tmpVars = array("/{CONTENT}/i", "/{MESSAGE}/i", "/{FORM_CONNEXION}/i");
			$replace = array("", $message, $form_connexion);
			$page = new Page($mainDir, $templateFile, "");
			$page->replace_tags($tmpVars, $replace);
			echo $page->output();
			exit;
			break;
			
		
		
		case "logout":
			session_destroy();
			$provider = $_SESSION["user"]["provider"];
			if($provider != "internal")
			{
				try 
				{
					$hybridauth = new Hybrid_Auth( _VHOST_PATH . '/classes/hybridauth/hybridauth/config.php' );
					$adapter = $hybridauth->getAdapter( $provider );
					$adapter->logout();
				}
				catch( Exception $e )
				{
					echo "Error: please try again!";
					echo "Original error message: " . $e->getMessage();
				}
			}
			header('location:index.php');
			break;
		
		
			
		
	}
?>