<?php
/* 
 * @package template
 * @author Thierry Cazalet
 * @Company phracktale
 * @copyright 2003 - 2011 Thierry Cazalet
 *
 * Divers utilitaires
 *
 */
	class Utils
	{
		Private $db;

		function __construct($value)
		{
				$this->db = $value;
		}
		
		function findRegion($db, $value)
		{
			
			$code = substr($value, 0, 2);
			$T09 = new T09_regions($db);
			
			$result = $T09->findRegion($code);
			$region = $result[0];
			return $region->r_code;
		}
		
		function findRegionID($value)
		{
			$code = substr($value, 0, 2);
			$T09 = new T09_regions($this->db);
			$result = $T09->findRegionByID($code);
			if(count($result) > 0)
			{
				$region = $result[0];
				return $region->T09_codeinterne_i;
			}
		}
		
		function getGroupe($effectif)
		{
			$T14 = new T14_groupe($this->db);
			$groupes = $T14->selectAllActive();
			
			foreach($groupes as $groupe)
			{
				eval('$regle = (' .$groupe->T14_regle_va .');');
				if($regle)
				{
					if($groupe->T14_code_va == "PPME" && $_SESSION['user']['is_ppme'] == 0)
					{
						return false;
						exit;
					}
					else
					{
						return $groupe->T14_codeinterne_i;
						exit;
					}
				}
			}
			return false;
		}
	}

?>