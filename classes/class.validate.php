<?php
Class Validate
{
	var $type;
	
	public function __construct()
	{
		
	} 
	
	
	
	public function testFields($array)
	{
		foreach($array as $var => $field)
		{
			
			$value = $field['value'];
			$type = $field['type'];
			$minLength = $field['minLength'];
			$maxLength = $field['maxLength'];
			$required = $field['required'];
			
			/* Test valeur obligatoire */
			if (isset($field['required']))
			{
				if($required == "true" && $value == ""){$erreur[$var]['required'] = "Cette valeur est obligatoire";}
			}
			
			/* Test taille minimale */
			if (isset($field['minLength']))
			{
				if(strlen($value) < $minLength ){$erreur[$var]['minLength'] = "Trop court";}
			}
			
			/* Test taille maximale */
			if (isset($field['maxLength']))
			{
				if(strlen($value) > $maxLength ){$erreur[$var]['maxLength'] = "Trop grand";}
			}
			
			/* Test du type de variable */
			if (isset($field['type']))
			{
				
				switch($type)
				{
					case 'int':
						if(!is_int($value * 1)){$erreur[$var]['type'] = "ce n'est pas un entier";}
						break;
					
					case 'numeric':
						if(!is_numeric($value * 1)){$erreur[$var]['type'] = "ce n'est pas une valeur numérique";}
						
						break;
						
					case 'string':
						if(!is_string($value)){$erreur[$var]['type'] = "ce n'est pas une chaîne";}
						break;
						
					case 'date':
						
						break;
						
					case 'email':
						
						break;
				}
			}
			
			if (count($erreur[$var]) > 0 )
			{
				$ret[$var]['text'] = '<ul class="error">';
				foreach($erreur[$var] as $code => $err)
				{
					$ret[$var]['text'] .= "<li>" . $err . "</li>";
				}
				$ret[$var]['text'] .= "</ul>";
				$ret[$var]['class'] = " error";
			}
		}
		
		return $ret;
	}
}
	