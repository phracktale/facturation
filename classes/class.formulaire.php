<?php

/**
 *
 *
 *
 *
 *
 */
Class Formulaire
{
	var $lieu;
	var $table_name;
	var $fields;
	var $codeinterne;
	var $codeinterne_field;
	var $locale;
	var $redirection;
	var $staticInput;
	var $update;
	
	
	public function __construct($db)
	{
		$this->db = $db;
		$this->table_name = "";
		$this->fields = array();
		$this->codeinterne=0;
		$this->codeinterne_field=0;
		$this->locale = "FR";
		$this->redirection = null;
		$this->staticInput = null;
		$this->update = false;
	}
	
    /**
     * 
     * 
     * @param <type> $value 
     * @return nothing
     */
	public function setLocale($value)
	{
		$this->locale = $value;
	}
	
	/**
     * 
     * 
     * @param <type> $value 
     * @return nothing
     */
	public function setStaticInput($key, $value)
	{
		$this->staticInput[$key] = $value;
	}
	
	 /**
     * 
     * 
     * @param <type> $value 
     * @return nothing
     */
	public function setRedirection($value)
	{
		$this->redirection = $value;
	}
	
    /**
     * Chargement du fichier xml
     * 
     * @param string $xml_file 
     * @return nothing
     */
	public function load($xml_file)
	{
		$xml = new DomDocument();
		$xml->preserveWhiteSpace = false;
		
		$xml->load($xml_file . '.xml');
		$xml->formatOutput = true;
		$racine = $xml->documentElement;
		
		$this->table_name = $racine->getAttribute("table_name");
		$this->codeinterne_field = $racine->getAttribute("codeinterne_field");
		
		$listFields = $racine->getElementsByTagName("field");
		
		foreach($listFields as $field)
		{
			$this->fields[$field->getAttribute("name")] = array(
				"function" => $field->getAttribute("function"),
				"name" => $field->getAttribute("name"),
				"data" => $field->getAttribute("data"),
				"type" => $field->getAttribute("type"),
				"encrypt" => $field->getAttribute("encrypt")
			);
			if($field->getAttribute("function") == "dico")
			{
				$this->fields[$field->getAttribute("name")]["source"] =  $field->getAttribute("source");
				$this->fields[$field->getAttribute("name")]["code"] =  $field->getAttribute("code");
				$this->fields[$field->getAttribute("name")]["section"] =  $field->getAttribute("section");
				$this->fields[$field->getAttribute("name")]["section_value"] =  $field->getAttribute("section_value");
				$this->fields[$field->getAttribute("name")]["key"] =  $field->getAttribute("key");
				$this->fields[$field->getAttribute("name")]["field"] =  $field->getAttribute("field");
				$this->fields[$field->getAttribute("name")]["locale"] =  $field->getAttribute("locale");
			}
			
			if($field->getAttribute("function") == "external")
			{
				$this->fields[$field->getAttribute("name")]["codeinterne"] =  $field->getAttribute("codeinterne");
				$this->fields[$field->getAttribute("name")]["field"] =  $field->getAttribute("field");
				$this->fields[$field->getAttribute("name")]["source"] =  $field->getAttribute("source");
			}
			
			$errors = $field->getElementsByTagName('error');
			foreach($errors as $error)
			{
				$this->fields[$field->getAttribute("name")]['error']['className'] = $error->getAttribute('className');
				
				$conditions = $error->getElementsByTagName('condition');
				$k = 0;
				foreach($conditions as $condition)
				{
					$this->fields[$field->getAttribute("name")]['error']['conditions'][$k]['test'] = $condition->getAttribute('test');
					$this->fields[$field->getAttribute("name")]['error']['conditions'][$k]['value'] = $condition->getAttribute('value');
					
					$messages = $condition->getElementsByTagName('message');
					foreach($messages as $message)
					{
						if ($message->getAttribute('locale') == $this->locale)
						{
							$this->fields[$field->getAttribute("name")]['error']['conditions'][$k]['message'] = $message->nodeValue;
						}
					}
					
					$k++;
				}
			}
		}
	}
	

    /**
     * 
     * 
     * @param <type> $codeinterne
     * @return <type>
     */
	public function get($codeinterne)
	{
		// on charge les donn�es du formulaire en fonction du codeinterne fourni (input, textarea, ...)
		// <field name="codepostal" function="internal" data="T13_codepostal_va" type="input">
		$query = "SELECT ";
		foreach($this->fields as $field_name => $field)
		{
			$query .= " {$this->table_name}.{$field['data']}, ";
			$data[$field['data']]['type'] = $field['type'];
		}
		
		$query = 	rtrim($query, ", ");
		$query .= " FROM {$this->table_name} WHERE {$this->codeinterne_field}={$codeinterne};";
		$result = $this->db->query($query);
		if($result)
		{
			$row = $result->fetch(PDO::FETCH_ASSOC);
			foreach($row as $name => $value)
			{
				$data[$name]['value'] = $value;			
			}
		}
		
		// On traite Les functions externes de donn�es (select / radio boutons / checkbox)
		// <field name="effectif" function="t10_dico" data="T10_code_effectif_va"  section="effectif" codeinterne="T12_codeinterne_i"  key="T12_code_va" field="T12_libelle_va" type="select">
		foreach($this->fields as $field_name => $field)
		{
			if ($field['function'] == "dico")
			{
				$query = "SELECT {$field['key']} as code, {$field['field']} as value FROM {$field['source']} where {$field['locale']}=\"{$this->locale}\" AND {$field['section']}=\"{$field['section_value']}\";";
				$result = $this->db->query($query);
				$rows = $result->fetchAll(PDO::FETCH_ASSOC);
				if(count($rows) > 0)
				{
					$aData = array();
					foreach($rows as $row)
					{
						$aData[$row['code']] =  $row['value'];
					}
					
					$data[$field['data']]['options'] = $aData;
				}
			}
			
			if ($field['function'] == "external")
			{
				$query = "SELECT {$field['codeinterne']} as code, {$field['field']} as value FROM {$field['source']}";
				$result = $this->db->query($query);
				$rows = $result->fetchAll(PDO::FETCH_ASSOC);	
				if(count($rows) > 0)
				{
					$aData = array();
					foreach($rows as $row)
					{
						$aData[$row['code']] =  $row['value'];
					}
					$data[$field['data']]['options'] = $aData;
				}
			}
		}
		
		/*
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		*/
		return $data;
	}
	
	/**
     * Mise � jour  des valeurs post�es du formulaire et traitement des erreurs
     * 
     * @param <type> $codeinterne 
     * @return array
     */
	public function update($codeinterne = "NULL")
	{
		$this->update = true;
		$this->save($codeinterne);
	}
	
    /**
     * Sauvegarde des valeurs post�es du formulaire et traitement des erreurs
     * 
     * @param <type> $codeinterne 
     * @return array
     */
	public function save($codeinterne = "NULL")
	{
		$error=0;
		$retour = array();
		$this->codeinterne = $codeinterne;
		
		$query = "select count(*)nb from {$this->table_name} where {$this->codeinterne_field}={$codeinterne};";
		
		$result = $this->db->query($query);
		if($result)
		{
			$row = $result->fetch(PDO::FETCH_OBJ);
			$nb = $row->nb;
		}
		
		if(count($this->staticInput) > 0)
		{
			foreach($this->staticInput as $name => $value)
			{
				$fields .= "{$name}, ";
				$values .= "'{$value}', ";
			}
		}
			
		if($nb == 0 || $codeinterne == "NULL")
		{
			$query = "insert into  {$this->table_name} ";

			$fields .= "{$this->codeinterne_field}, ";
			$values .= "NULL, ";
			
			
			foreach($this->fields as $field_name => $field)
			{
				$value = $_POST[$field_name];
				if($field['function'] != "verification")
				{
					if ($field['encrypt'] != "")
					{
						eval('$value = ' . $field['encrypt'] . '($value);');
					}
					$datas[] = $value;
					$fields .= "{$field['data']}, ";
					$values .= "?, ";
					
				}
				$retErreur = $this->getError($field, $value);
				
				
				$retour['profil'][$field['data']]['value'] = $value;
				
				if($retErreur != "")
				{
					$retour['error'][$field['data']]['value'] = '<span class="' . $field['error']['className'] . '">' . $retErreur['message'] . '</span>';
					$retour['error'][$field['data']]['class'] = $field['error']['className'];
					$error++;
				}
			}
			
		
			$fields = rtrim($fields, ", ");
			$values = rtrim($values, ", ");
			
			$query .= " ({$fields}) VALUES ({$values});";
		
		}
		else
		{		
			$query = "update {$this->table_name} set ";
			$ssquery = "";
			
			foreach($this->fields as $field_name => $field)
			{
				$value = $this->ecranXss($_POST[$field_name]);
				if($field['function'] != "verification")
				{
					$datas[] = $value;
					$ssquery .= "{$field['data']}=?, ";
				}
				$retErreur = $this->getError($field, $value);
				if($retErreur != "")
				{
					
					$retour['error'][$field['data']]['value'] = '<span class="' . $field['error']['className'] . '">' . $retErreur['message'] . '</span>';
					$retour['error'][$field['data']]['class'] = $field['error']['className'];
					$error++;
				}
			}
			
			$query .= rtrim($ssquery, ", ");
			$query .= " where {$this->codeinterne_field}={$codeinterne};";

			
		}

		/* Traitement des erreurs */

		
		if($error == 0)
		{
			$sth = $this->db->prepare($query);
			$sth->execute($datas);
			$retour['codeinterne'] =  $this->db->lastInsertId();
			
			if($this->redirection != "")
			{
				header('location:' . $this->redirection);
			}
		}
		
		return $retour;
	}
	
    /**
     * 
     * 
     * 
     * @return <type>
     */
	public function getFields()
	{
		return $this->fields;
	}
	
    /**
     * 
     * 
     * @param <type> $field 
     * @param <type> $value 
     * 
     * @return <type>
     */
	private function getError($field, $value)
	{
		
		foreach($field['error']['conditions'] as $condition)
		{
			switch ($condition['test'])
			{
				case "obligatoire":
					if($value == "")
					{
						$error['message'] = $condition['message'];
					}
					break;
					
				case "max":
					if(strlen($value) > $condition['value'])
					{
						$error['message'] = $condition['message'];
					}
					break;
					
				case "unique":
					if(!$this->update)
					{
						$query = "SELECT count(*) nb FROM {$this->table_name} WHERE {$field['data']}='{$value}'";
						$result = $this->db->query($query);
						$row = $result->fetch(PDO::FETCH_OBJ);
			
						if($row->nb > 0)
						{
							$error['message'] = $condition['message'];
						}
					}
					break;
					
				case "verification":
					if($_POST[$field['name']] !=  $_POST[$condition[value]])
					{
						$error['message'] = $condition['message'];
					}
					break;
			}
		}
		return $error;
	}
	
	public function getTemplateFormVars($form, $error = null, $mode = "form")
	{
		$fields = $this->getFields();
		
		foreach($fields as $field_name => $field)
		{
			if( $field['type'] == "input" || $field['type'] == "textarea" || $field['type'] == "password"|| $field['type'] == "hidden")
			{
				$tmpVars[] = "/{{$field['data']}}/i";
				$replace[] = $form[$field['data']]['value'];
			}
			
			if(( $field['type'] == "select" || $field['type'] == "radio" || $field['type'] == "checkbox" ))
			{
				$options = $form[$field['data']]['options'];
				$items = "";
				
				
				foreach($options as $id => $option)
				{
					if($mode == "form")
					{
						switch($field['type'])
						{
							case "select":
								if($id == $form[$field['data']]['value']) {$selected = ' selected="selected"';}else{$selected = '';}
								$items .= "<option value=\"{$id}\" {$selected}>{$option}</option>";
								break;
							
							case "radio":
								if($id == $form[$field['data']]['value']) {$selected = ' selected="selected"';  }else{$selected = '';}
								$items .= "<input type =\"radio\" name=\"{$field['name']}\" value=\"{$id}\" {$selected} /><label for=\"{$field['name']}\">{$option}</label>";
								break;
								
							case "checkbox":
								if($id == $form[$field['data']]['value']) {$selected = ' checked="checked"';}else{$selected = '';}
								$items .= "<input type =\"checkbox\" name=\"{$field['name']}[]\" value=\"{$id}\" {$selected} /><label for=\"{$field['name']}\">{$option}</label>";
								break;
						}
					}
					elseif($mode == "view")
					{
						if($id == $form[$field['data']]['value']) {$items = $option;}
					}
				}
				$tmpVars[] = "/{{$field['data']}}/i";
				$replace[] = $items;
				
			}
			
			
			if($error != "")
			{
				$error_value=$error[$field['data']]['value'];
				$error_class=$error[$field['data']]['class'];
			}
			
			$tmpVars[] = "/{ERROR_{$field_name}}/i";
			$replace[] = $error_value;
			
			$tmpVars[] = "/{class_ERROR_{$field_name}}/i";
			$replace[] = $error_class;
		}
		return array(
						"tmpVars" => $tmpVars,
						"replace" => $replace
		);
	}
	
	private function ecranXss($val)
	{
		$val = strip_tags($val);
		$val = htmlspecialchars($val, ENT_COMPAT, "utf-8");
		return $val;
	}
}