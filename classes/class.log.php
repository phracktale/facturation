<?php
	/**
	 * Log des actions
	 * 
	 * @name phracktale
	 * @author Thierry Cazalet <phracktale@gmail.dom> 
	 * @link  http://www.phracktale.com
	 * @copyright phracktale 2009-2011
	 * @version 1.0.0
	 * @package macif
	 */ 
	
	/*
	 * classe permettant de loguer les actions
	 */
	 
	
	
	Class Log
	{
		private $ip;
		private $idUser;
		private $hostname;
		private $roleUser;
		
		public function __construct($db)
		{
			$this->db = $db;
			$this->ip = $_SERVER['REMOTE_ADDR'];
			$this->idUser = $_SESSION['user']['id_user'];
			$this->roleUser = $_SESSION['user']['groupe'];
			$this->hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		} 
		
		public function saveLog($action, $T03_codeinterne_i=0)
		{
			$query = "insert into t05_log (T01_codeinterne_i, T03_codeinterne_i, T05_role_va, T05_date_d, T05_contexte_va, T05_action_va, T05_ip_va, T05_hostname_va) values (?, ? ,? ,? ,? ,?, ?, ?);";
			$datas = array($this->idUser, $T03_codeinterne_i, $this->roleUser, date("Y-m-d h:m:s"), serialize($_REQUEST), $action, $this->ip, $this->hostname);
			
			$sth = $this->db->prepare($query);
			$result = $sth->execute($datas);
		}
		
		public function __destruct()
		{
    
		} 
	
	}