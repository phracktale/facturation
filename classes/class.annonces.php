<?php

Class Annonces
{
	var $lieu;
	var $recherche;
	var $contrats;
	var $rayon;
	var $pageCurrent;
	var $resultsMax;
	var $pageCount;
	var $nbResults;
	var $typeAnnonce;
	var $status;
	var $departement;
	var $region;
	var $locale;
	var $userID;
	var $mode;
	
	
	public function __construct($db)
	{
		$this->db = $db;
		$this->rayon = 30000;
		$this->pageCurrent = 0;
		$this->resultsMax = 10;
		$this->pageCount = 0;
		$this->nbResults = 0;
		$this->typeAnnonce = 2;
		$this->status = array(1, 4);
		$this->departement = "";
		$this->region = "";
		$this->locale = "FR";
		$this->userID = null;
		$this->mode = 1;
	}
	
	public function setMode($value)
	{
		/*
				$this->mode = null => liste des annonces dans le flux normal
				$this->mode = 1 => liste des annonces répondu par l'utilisateur
				$this->mode = 2 => liste des annonces remportées par l'utilisateur
				
		*/
		
		$this->mode = $value;
	}
	
	public function setLocale($value)
	{
		$this->locale = $value;
	}
	
	public function setUserID($value)
	{
		$this->userID = $value;
	}
	
	public function  getTypesContrats()
	{
		global $T04;
		return $T04->selectAll();
	}
	
	
	public function setCurrentPage($cur)
	{
		$this->pageCurrent = $cur;
	}
	
	public function setMaxResults($val)
	{
		$this->resultsMax = $val;
	}
	
	public function setTypeAnnonce($val)
	{
		$this->typeAnnonce = $val;
	}
	
	function setRayon($val)
	{
		$this->rayon = $val;
	}
	
	public function setLieu($val)
	{
		
		$this->lieu = $this->adress_parser ( $val );
		
	}
	
	public function setDepartement($val)
	{
		$this->departement = $val;
	}
	
	public function setRegion($val)
	{
		$this->region = $val;
	}
	
	public function setRecherche($val)
	{
		$this->recherche = $val;
	}
	
	public function setContrats($val)
	{
		$this->contrats = $val;
	}
	
	public function setStatus($val)
	{
		if($val == "")
		{
			$this->status = array(1, 4);
		}
		else
		{
			$this->status = $val;
		}
	}
	
	public function getAnnonces()
	{
		global $T03, $getGeoAnnonces;
		
		$getGeoAnnonces = "SELECT t03.T03_codeinterne_i, t03.T01_codeinterne_i,  t03.T05_codeinterne_i,  t03.T08_codeinterne_i, t03.T12_code_budget_i, t12a.T12_libelle_va as budget,  t03.T03_entreprise_va,  t03.T03_contact_va, t03.T03_email_va,  t03.T03_titre_va,  t03.T03_description_va,  t03.T03_coordonnees_va,  t03.T03_codepostal_va,  t03.T03_ville_va, t03.T03_latitude_n,  t03.T03_longitude_n,  t03.T03_date_debut_d,  t03.T03_date_fin_d,  t03.T03_uniqid_va,  t04.T04_codeinterne_i, t04.T04_titre_va, t05.T05_titre_va ";
		if ($this->userID > 0)
		{
			$getGeoAnnonces .= " ,t11.T01_codeinterne_i user_id, t11.T03_codeinterne_i ann_id, t11.T11_budget_va, t11.T11_duree_i ";
		}
		
		if($this->lieu['lat'] != "" && $this->lieu['lng'] != "")
		{
			$getGeoAnnonces .= " , get_distance_metres(" . $this->lieu['lat']. ", " .  $this->lieu['lng'] . ", T03_latitude_n, T03_longitude_n)  AS proximite  ";
		}	
			 
		$getGeoAnnonces .= " FROM  t03_annonces t03 ";
		
		if ($this->mode == 1 || $this->mode == 2 )
		{
			$getGeoAnnonces .= " right join t11_reponses t11 on t03.T03_codeinterne_i=t11.T03_codeinterne_i ";
		}
		$getGeoAnnonces .= " LEFT JOIN t09_annonces_has_typecontrat t09 ON t09.T03_codeinterne_i = t03.T03_codeinterne_i LEFT JOIN  t04_typecontrat t04 ON t04.T04_codeinterne_i=t09.T04_codeinterne_i ";
		$getGeoAnnonces .= " LEFT JOIN  t05_status t05 ON t05.T05_codeinterne_i=t03.T05_codeinterne_i ";
		$getGeoAnnonces .= " LEFT JOIN  t12_dico t12a ON t03.T12_code_budget_i=t12a.T12_codeinterne_i ";
		
		if($this->recherche != "")
		{
			$getGeoAnnonces .= " LEFT JOIN t07_annonces_has_mots_cles t07 ON t07.T03_codeinterne_i=t03.T03_codeinterne_i LEFT JOIN  t06_mots_cles t06 ON t06.T06_codeinterne_i=t07.T06_codeinterne_i ";
		}
		
		$getGeoAnnonces .= " LEFT JOIN t10_communes t10 ON t10.T10_codepostal_va=t03.T03_codepostal_va ";
		
		$getGeoAnnonces .= " WHERE t12a.T12_locale_va='" . $this->locale . "' AND T08_codeinterne_i = " . $this->typeAnnonce;
		
		if (($this->mode == 1 || $this->mode == 2)&& $this->userID > 0 )
		{
			$getGeoAnnonces .= " and t11.T01_codeinterne_i ={$this->userID} ";
		}
		
		if ($this->mode == 2 && $this->userID > 0)
		{
			$getGeoAnnonces .= " and t03.T01_codeinterne_i ={$this->userID} ";
		}
		
		if(count($this->status ) > 0 && $this->mode == "")
		{
			$liste_status .= " AND t05.T05_codeinterne_i IN (";
		
			foreach ($this->status as $s)
			{
				$liste_status .= $s . ", ";
			}
			$liste_status = rtrim($liste_status, ", ");
		
			$liste_status .= ")  ";
			$getGeoAnnonces .= $liste_status;
			
		}
		
		if($this->recherche != "")
		{	
			$phonex = new Phonex();
			$recherche = explode(" ", nettoie_chaine($this->recherche));
			$aPhonex = "";
			
			foreach($recherche AS $motcle)
			{
				$aPhonex .= "'" . $phonex->get($motcle) ."',";
			}
			
			$aPhonex = rtrim($aPhonex, ",");
			$getGeoAnnonces .= "AND T06_phonex_va IN (" . $aPhonex . ")";
		}
		
		if(count($this->contrats) > 0)
		{	
			foreach($this->contrats AS $contrat)
			{
				
				$lContrats .= $contrat.",";
			}
			$lContrats = rtrim($lContrats, ",");
			
			$getGeoAnnonces .= " AND t04.T04_codeinterne_i IN (" . $lContrats . ")";
		}
		
		if($this->region != "")
		{
			$getGeoAnnonces .= " AND t10.T10_coderegion_va=='" . $this->region . "'";
		}
		
		if($this->departement != "")
		{
			$getGeoAnnonces .= " AND SUBSTRING(t10.T10_codepostal_va, 1, 2)='" . $this->departement . "'";
		}
		
		$getGeoAnnonces .= " group by t03.T03_codeinterne_i  ";
		
		if($this->lieu['lat'] != "" && $this->lieu['lng'] != "")
		{	
			$getGeoAnnonces .= "  having proximite < " . $this->rayon;
		}
		
			
		$getGeoAnnonces .= " ORDER BY  ";
			
		if($this->lieu['lat'] != "" && $this->lieu['lng'] != "")
		{	
			$getGeoAnnonces .= " proximite, ";
		}	
		$getGeoAnnonces .= " T03_date_debut_d ";
		
		$result = $T03->getGeoAnnonces();
		
		$this->nbResults = count($result);
		$this->pageCount = ceil($this->nbResults/$this->resultsMax);
		
		$getGeoAnnonces .= " DESC LIMIT " . ( ($this->pageCurrent - 1 )* $this->resultsMax ) . ", " . $this->resultsMax;
		echo "<!--" . $getGeoAnnonces . "-->";
		$pagination = $this->getPagination();
		
		$debug = "";
		$annonces = $T03->getGeoAnnonces();
		
		return array('annonces' => $annonces, "pagination" => $pagination, "debug" => $debug );
		
	}
	
	public function getPagination($url)
	{
		$adj = 1;
		$link = strpos($url , "?") === false ? "?cur=" : "&cur=";

		// Initialisation des variables
		$prev = $this->pageCurrent - 1; // numéro de la page précédente
		$next = $this->pageCurrent + 1; // numéro de la page suivante
		$penultimate = $this->pageCount - 1; // numéro de l'avant-dernière page
		$pagination = ''; // variable retour de la fonction : vide tant qu'il n'y a pas au moins 2 pages

		if ($this->pageCount > 1) {
			$pagination .= '<ul class="pagination_container row w100">';
		

			/* =================================
			 *  Affichage du bouton [précédent]
			 * ================================= */
			if ($this->pageCurrent > 1) {
				$pagination .= "<li class=\"col txtleft\"><a class=\"bouton bouton_violet\" href=\"{$url}{$link}{$prev}\">Retour</a> </li>";
			}

			/**
			 * Début affichage des pages, l'exemple reprend le cas de 3 numéros de pages adjacents (par défaut) de chaque côté du numéro courant
			 * - CAS 1 : il y a au plus 12 pages, insuffisant pour faire une troncature
			 * - CAS 2 : il y a au moins 13 pages, on effectue la troncature pour afficher 11 numéros de pages au total
			 */

			/* ===============================================
			 *  CAS 1 : au plus 12 pages -> pas de troncature
			 * =============================================== */
			if ($this->pageCount < 7 + ($adj * 2)) {
				$pagination .= '<li class="col txtcenter w100">';
				// Ajout de la page 1 : on la traite en dehors de la boucle pour n'avoir que index.php au lieu de index.php?p=1 et ainsi éviter le duplicate content
				$pagination .= ($this->pageCurrent == 1) ? '<b class="pagination_active">1</b>' : " <a class=\"pagination\" href=\"{$url}\">1</a> "; // Opérateur ternaire : (condition) ? 'valeur si vrai' : 'valeur si fausse'

				// Pour les pages restantes on utilise itère
				for ($i=2; $i<=$this->pageCount; $i++) {
					if ($i == $this->pageCurrent) {
						// Le numéro de la page courante est mis en évidence (cf. CSS)
						$pagination .= "<b class=\"pagination_active\">{$i}</b>";
					} else {
						// Les autres sont affichées normalement
						$pagination .= "<a class=\"pagination\"  href=\"{$url}{$link}{$i}\">{$i}</a>";
					}
				}
				$pagination .= '</li>';
			}
			/* =========================================
			 *  CAS 2 : au moins 13 pages -> troncature
			 * ========================================= */
			else {
				/**
				 * Troncature 1 : on se situe dans la partie proche des premières pages, on tronque donc la fin de la pagination.
				 * l'affichage sera de neuf numéros de pages à gauche ... deux à droite
				 * 1 2 3 4 5 6 7 8 9 … 16 17
				 */
				 $pagination .= '<li class="col txtcenter w100">';
				if ($this->pageCurrent < 2 + ($adj * 2)) {
					// Affichage du numéro de page 1
					$pagination .= ($this->pageCurrent == 1) ? "<b class=\"pagination_active\">1</span>" : "<a  class=\"pagination\" href=\"{$url}\">1</a>";

					// puis des huit autres suivants
					for ($i = 2; $i < 4 + ($adj * 2); $i++) {
						if ($i == $this->pageCurrent) {
							$pagination .= "<b class=\"pagination_active\">{$i}</b>";
						} else {
							$pagination .= "<a class=\"pagination\"  href=\"{$url}{$link}{$i}\">{$i}</a>";
						}
					}

					// ... pour marquer la troncature
						$pagination .= "<b class=\"pagination_active\">&hellip;</b>";

					// et enfin les deux derniers numéros
					$pagination .= "<a  class=\"pagination\" href=\"{$url}{$link}{$penultimate}\">{$penultimate}</a>";
					$pagination .= "<a  class=\"pagination\" href=\"{$url}{$link}{$this->pageCount}\">{$this->pageCount}</a>";
				}
				/**
				 * Troncature 2 : on se situe dans la partie centrale de notre pagination, on tronque donc le début et la fin de la pagination.
				 * l'affichage sera deux numéros de pages à gauche ... sept au centre ... deux à droite
				 * 1 2 … 5 6 7 8 9 10 11 … 16 17
				 */
				elseif ( (($adj * 2) + 1 < $this->pageCurrent) && ($this->pageCurrent < $this->pageCount - ($adj * 2)) ) {
					// Affichage des numéros 1 et 2
					$pagination .= "<a class=\"pagination\"  href=\"{$url}\">1</a>";
					$pagination .= "<a  class=\"pagination\" href=\"{$url}{$link}2\">2</a>";
						$pagination .= "<b class=\"pagination_active\">&hellip;</b>";

					// les pages du milieu : les trois précédant la page courante, la page courante, puis les trois lui succédant
					for ($i = $this->pageCurrent - $adj; $i <= $this->pageCurrent + $adj; $i++) {
						if ($i == $this->pageCurrent) {
							$pagination .= "<b class=\"pagination_active\">{$i}</b>";
						} else {
							$pagination .= "<a  class=\"pagination\" href=\"{$url}{$link}{$i}\">{$i}</a>";
						}
					}

					$pagination .= "<b class=\"pagination_active\">&hellip;</b>";

					// et les deux derniers numéros
					$pagination .= "<a  class=\"pagination\" href=\"{$url}{$link}{$penultimate}\">{$penultimate}</a>";
					$pagination .= "<a  class=\"pagination\" href=\"{$url}{$link}{$this->pageCount}\">{$this->pageCount}</a>";
				}
				/**
				 * Troncature 3 : on se situe dans la partie de droite, on tronque donc le début de la pagination.
				 * l'affichage sera deux numéros de pages à gauche ... neuf à droite
				 * 1 2 … 9 10 11 12 13 14 15 16 17
				 */
				else {
					// Affichage des numéros 1 et 2
					$pagination .= "<a  class=\"pagination\" href=\"{$url}\">1</a>";
					$pagination .= "<a  class=\"pagination\" href=\"{$url}{$link}2\">2</a>";
						$pagination .= "<b class=\"pagination_active\">&hellip;</b>";

					// puis des neuf derniers numéros
					for ($i = $this->pageCount - (2 + ($adj * 2)); $i <= $this->pageCount; $i++) {
						if ($i == $this->pageCurrent) {
							$pagination .= "<b class=\"pagination_active\">{$i}</b>";
						} else {
							$pagination .= "<a class=\"pagination\" href=\"{$url}{$link}{$i}\">{$i}</a>";
						}
					}
				}
				
				$pagination .= '</li>';
			}

			/* ===============================
			 *  Affichage du bouton [suivant]
			 * =============================== */

				
			if($this->pageCurrent<($this->pageCount)){$pagination .= " <li class=\"col txtleft\"><a class=\"bouton bouton_violet right col w25 txtright\" href=\"{$url}{$link}{$next}\">Suite</a> </li>";}
				
			$pagination .= '</ul>';
		}

		return ($pagination);
	}
	
	public function getFormBudget()
	{
		global $T12;
		$budgets = $T12->getLibelle($this->locale, "budget");
		$field_budget = '<select name="budget" id="budget">';
		foreach ($budgets as $budget)
		{
			$field_budget .= "<option value=\"{$budget->T12_codeinterne_i}\">{$budget->T12_libelle_va}</option>";
		}
		$field_budget .= '</select>';
		return $field_budget;
	}
	
	public function getFormTypesContrats($liste_checked = array(), $type="checkbox")
	{
		$contrats = $this->getTypesContrats();
		if(count($contrats) >0 )
		{
			if($type == "checkbox" || $type == "radio")
			{
				$types_contrats = '<ul class="liste_contrats">';
			}
			elseif($type == "select")
			{
				$types_contrats = '<br /><select name="type_contrat" id="type_contrat">';
			}
			$k = 1;
			foreach($contrats as $contrat)
			{
				$selected = "";
				
				foreach($liste_checked as $check){
					if ($check == $contrat->T04_codeinterne_i)
					{
						if($type=="select"){$selected = 'selected="selected"';}
						if($type=="checkbox"){$selected = 'checked="checked"';}
						if($type=="radio"){$selected = 'checked="checked"';}
					}
				}
				
				if($type == "checkbox" || $type == "radio")
				{
					$types_contrats .= '<li><input type="' . $type . '" id="type_contrat_'.$k.'" name="type_contrat[]" class="type_contrat" value="'. $contrat->T04_codeinterne_i .'" ' . $selected . '/><label for="type_contrat_'.$k.'" class="inline"><span class="ui-icon icon-categorie" ></span>'. $contrat->T04_titre_va .'</label></li>';
				}
				elseif($type == "select")
				{
					$types_contrats .= '<option value="'. $contrat->T04_codeinterne_i .'" ' . $selected . '>'. $contrat->T04_titre_va .'</option>';
				}
				
				$k++;
				
			}
			if($type == "checkbox" || $type == "radio")
			{
				$types_contrats .= "</ul>";
			}
			elseif($type == "select")
			{
				$types_contrats .= '</select>';
			}
		}
		return $types_contrats;
	}
	
	public function getAnnoncesFiltre()
	{
		$result = $this->getAnnonces();
		$liste_annonces = $result['annonces'];
		$pagination = $result['pagination'];
		$debug = $result['debug'];

		if(count($liste_annonces) > 0)
		{
			$resultat =  '<div id="liste_reponses">';
			foreach ($liste_annonces as $annonce)
			{
				$url = 'annonce-' . $annonce->T03_codeinterne_i . '_' . nettoie_chaine($annonce->T03_titre_va, $separateur="-");
				$titre = stripslashes($annonce->T03_titre_va);
				$date = $this->datediff($annonce->T03_date_debut_d);
				$contrat = $annonce->T04_titre_va;
				$budget = strip_tags($annonce->budget);
				$entreprise = $annonce->T03_entreprise_va;
				$contact = $annonce->T03_contact_va;
				$email = $annonce->T03_email_va;
				$mon_budget = $annonce->T11_budget_va;
				$ma_duree = $annonce->T11_duree_i;
				$pluriel = $ma_duree>1 ? "s" : "";
				$status = "";
				if($annonce->T05_codeinterne_i == 4)
				{
					$status = 'POURVUE';
				}
				
				$entreprise = $annonce->T03_entreprise_va;
				$code_postal = $annonce->T03_codepostal_va;
				$ville = $annonce->T03_ville_va;
				
				$resultat .= 	"<div class=\"reponse grid3\">
									<div>
										<a href=\"{$url}\">{$titre}</a> <br/><small>{$date} {$budget} {$status}</small>
										<br />{$entreprise} {$contact} [{$email}]
									</div>
									<div>
										Budget proposé : {$mon_budget} €HT<br/>Durée estimée : {$ma_duree} jour{$pluriel}
									</div>
									<div>
										<a href=\"editer_offre\" class=\"bouton bouton_small\">Editer mon offre</a>
									</div>
									<br class=\"nettoyeur\" \>
								</div>";
			
			}
			$resultat .= '</div>';
		}
		return array('annonces' => $resultat, 'pagination' => $pagination);
	}
	public function adress_parser ( $adresse ) 
	{ 
		//$geocode_url = signUrl("http://maps.googleapis.com/maps/api/geocode/json?address=" .  urlencode ( $adresse ). "&sensor=true&language=fr" . "&client=" . _GEOCODE_CLIENT, _GEOCODE_SIGNATURE);
		$geocode_url = "http://maps.googleapis.com/maps/api/geocode/json?address=" .  urlencode ( $adresse ). "&sensor=true&language=fr";
		$results = json_decode ( file_get_contents ( $geocode_url ), 1 ); 
		
		$parts = array( 
		'numero'		=> array ('street_number'),
		'adresse' 		=>	array( 'route' ), 
		'ville'			=> 	array( 'locality' ), 
		'departement' 	=>	array( 'administrative_area_level_2' ), 
		'region' 		=>	array( 'administrative_area_level_1' ), 
		'code_postal' 	=>	array( 'postal_code' ), 
		'pays' 	=>	array( 'country' ), 
		);
		
		$oAdresse['lat'] = $results [ 'results' ][ 0 ]['geometry']['location']['lat'];
		$oAdresse['lng'] = $results [ 'results' ][ 0 ]['geometry']['location']['lng'];
		
		if (!empty( $results [ 'results' ][ 0 ][ 'address_components' ])) { 
			$components = $results [ 'results' ][ 0 ][ 'address_components' ]; 
			foreach( $parts as $champ =>& $types ) { 
				foreach( $components as & $component ) { 
					if ( in_array ( $component [ 'types' ][ 0 ], $types )) 
					{
						$oAdresse [ $champ ] = array( 
														'libelle_court'		=>	$component [ 'short_name' ], 
														'libelle_long'  	=> 	$component [ 'long_name' ]
											);
					}
					elseif (empty( $oAdresse [ $champ ][ 'libelle_court' ]))
					{
						$oAdresse [ $champ ][ 'libelle_court' ] = '';
						$oAdresse [ $champ ][ 'libelle_long' ] = '' ; 
					}
				} 
			} 
		}
		
		return $oAdresse ; 
	}
	
	public function datediff($date)
	{
		$date1 = new DateTime($date); 
		$date2 = new DateTime("now"); 
		$interval = $date1->diff($date2); 
		
		$years = $interval->format('%y'); 
		$months = $interval->format('%m'); 
		$days = $interval->format('%d'); 
		$hours = $interval->format('%h'); 
		$mins = $interval->format('%i');
		if($years != 0){ 
			$ago = $years.' année'; 
		}else if($months != 0){ 
			$ago =  $months.' mois'; 
		}else if($days != 0){ 
			$ago = $days . ' jours';
		} else if($hours != 0){ 
			$ago = $hours.' heures';
		}  else if($mins != 0){ 
			$ago = $mins.' minutes';
		}
		else
		{
			$ago = " quelques instants";
		}
		return $ago;
	}
}
?>