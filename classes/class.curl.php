<?php
	class cURL { 
		var $headers; 
		var $user_agent; 
		var $compression; 
		var $cookie_file; 
		var $proxy; 
		
		public function __construct()
		{
			$this->headers = array();
		}
		
		function setHeaders($http_headers)
		{
			$this->headers = $http_headers;
		}
		
		function get($url, $vars = array()) {
			if (!empty($vars)) {
				$url .= (stripos($url, '?') !== false) ? '&' : '?';
				$url .= (is_string($vars)) ? $vars : http_build_query($vars, '', '&');
			}
			return $this->request('GET', $url);
		}
		
		function post($url, $data = array()) { 
			return $this->request("POST", $url, $data) ;
		} 
		
		function head($url,$data = array()) { 
			return $this->request("HEAD", $url, $data) ;
		} 
		
		function put($url,$data = array()) { 
			return $this->request("PUT", $url, $data) ;
		} 
		
		 function delete($url, $vars = array()) {
			return $this->request('DELETE', $url, $vars);
		}
		
		
		function request($method, $url, $data = array()) { 
		
			
			$process = curl_init($url); 
			// Bypasser la vérification SSL
			curl_setopt ($process, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt ($process, CURLOPT_SSL_VERIFYPEER, 0);
			
			/*
			curl_setopt($process, CURLOPT_SSL_VERIFYPEER, true);
			curl_setopt($process, CURLOPT_SSL_VERIFYHOST, 2);
			//curl_setopt($process, CURLOPT_CAINFO,  "/home/apparaitre/clients/hubwin-mamans/emploi/cert/hubwin-fr.pem");
			curl_setopt($process, CURLOPT_CAPATH,  "/etc/ssl/certs/");
			*/
			
			if(isset($_GET['auth']))
			{
				if($_GET['user'] && $_GET['pass']){
				   curl_setopt($process, CURLOPT_USERPWD, $_GET['user'] . ':' . $_GET['pass']);
					   curl_setopt($process, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
				}
			}
			curl_setopt($process, CURLOPT_HTTPHEADER, $this->headers); 
			curl_setopt($process, CURLOPT_HEADER, false); 

			
			curl_setopt($process, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
			
			switch (strtoupper($method)) {
				case 'HEAD':
					curl_setopt($process, CURLOPT_NOBODY, true);
					break;
				case 'GET':
					curl_setopt($process, CURLOPT_HTTPGET, true);
					break;
				case 'POST':
					curl_setopt($process, CURLOPT_POST, true);
					curl_setopt($process, CURLOPT_POSTFIELDS, $data); 
					break;
				case "PUT":
					curl_setopt($process, CURLOPT_POSTFIELDS, $data); 
					
					break;
				default:
					curl_setopt($process, CURLOPT_CUSTOMREQUEST, $method);
			}
			
			$reponse = curl_exec($process);
			$http_code = curl_getinfo($process, CURLINFO_HTTP_CODE);
			$error = curl_error($process);
			
			curl_close($process); 
			return array('http_code' => $http_code, "reponse" => $reponse, 'error'=>$error); 
		} 	
		
		
		
		function error($error) { 
			echo "<center><div style='width:500px;border: 3px solid #FFEEFF; padding: 3px; background-color: #FFDDFF;font-family: verdana; font-size: 10px'><b>cURL Error</b><br>$error</div></center>"; 
			die; 
		} 
	} 

?>