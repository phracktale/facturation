<?php

class Phonex {

	private $all = array (
		"Ä|ä|À|à|Â|â" => "A",
		"Ë|ë|É|é|È|è|Ê|ê" => "E",
		"Ï|ï|Î|î" => "I",
		"Ö|ö|Ô|ô" => "O",
		"Ü|ü|Ù|ù|Û|û" => "U",
		"Ÿ|ÿ" => "Y",
		"Ç|ç" => "C",
		" |-" => "",
		"GUI" => "KI",
		"GUE" => "KE",
		"GA" => "KA",
		"GO" => "KO",
		"GU" => "K",
		"CA" => "KA",
		"CO" => "KO",
		"CU" => "KU",
		"Q" => "K",
		"CC" => "K",
		"CK" => "K",		
		"E|I|O|U" => "A",
		"^ASA" => "AZA",
		"^KN" => "NN",
		"^PF" => "FF",
		"^PH" => "FF",
		"^SCH" => "SSS",
		"([^C|S])H" => "\\1",
		"([^A])Y" => "\\1",
		"[A|T|D|S]$" => "",
		);
		
	private function replace($msg)
		{
		reset($this->all);
		while(list($k,$v) = each($this->all))
			{
			$msg = ereg_replace($k,$v,$msg);
			}
		return $msg;
		}
		
	private function unique($msg)
		{
		$box = $tmp = "";
		for($i=0;$i<strlen($msg);$i++)
			{
			if($box!=$msg[$i]) $tmp.= $msg[$i];
			$box = $msg[$i];
			}
		return $tmp;
		}
	
	public function get($msg)
		{
		$msg = strtoupper(trim($msg));	
		
		$msg = $this->replace($msg);					  
		$msg = ((substr($msg,0,1)=="A") ? "A":"").ereg_replace("A","",$msg);
		$msg = $this->unique($msg);
		
		return substr($msg."    ",0,4);
		}
	
}

?>