<?php
	$getNewNumOrder = "select nextNumOrder(?, ?) num_piece";
	$getPiecesByUser = "select t03.*, t05.* from t05_pieces_comptables t05 left join t03_clients t03 on t03.T03_codeinterne_i=t05.T03_codeinterne_i where t05.T06_codeinterne_i=? and t05.T01_codeinterne_i=? order by T05_numPiece_va desc";
	
	$getPieceByNumOrder = "select t03.*, t05.*, t10.T10_libelle_va from t05_pieces_comptables t05 left join t03_clients t03 on t03.T03_codeinterne_i=t05.T03_codeinterne_i left outer join t10_dico t10 on t05.T10_code_modeReglement_va=t10.T10_code_va where t10.T10_locale_va='" . $_SESSION['locale'] . "' and t05.T06_codeinterne_i=? and t05.T01_codeinterne_i=? and t05.T05_numPiece_va=? order by T05_numPiece_va desc";
	
	$getPieceByID = "select t03.*, t05.*, t10.T10_libelle_va from t05_pieces_comptables t05 left join t03_clients t03 on t03.T03_codeinterne_i=t05.T03_codeinterne_i left outer join t10_dico t10 on t05.T10_code_modeReglement_va=t10.T10_code_va where t10.T10_locale_va='" . $_SESSION['locale'] . "' and t05.T06_codeinterne_i=? and t05.T01_codeinterne_i=? and t05.T05_codeinterne_i=? order by T05_numPiece_va desc";
	
	$getDevisByID = "select t03.*, t05.*, t10.T10_libelle_va from t05_pieces_comptables t05 left outer join t10_dico t10 on t05.T10_code_modeReglement_va=t10.T10_code_va  left join t03_clients t03 on t03.T03_codeinterne_i=t05.T03_codeinterne_i  where  t05.T06_codeinterne_i=? and t05.T01_codeinterne_i=? and t05.T05_codeinterne_i=? order by T05_numPiece_va desc";
	
	$getPiecesFromParent = "select t03.*, t05.*, t10.T10_libelle_va from t05_pieces_comptables t05 left join t03_clients t03 on t03.T03_codeinterne_i=t05.T03_codeinterne_i left outer join t10_dico t10 on t05.T10_code_modeReglement_va=t10.T10_code_va where t10.T10_locale_va='" . $_SESSION['locale'] . "' and t05.T06_codeinterne_i=? and t05.T01_codeinterne_i=? and t05.T05_parent_piece_i=? order by T05_numPiece_va desc";
	
	$getSumMontantFacturesFromDevis = "select sum(T05_totalHT_n) total_acompte from t05_pieces_comptables t05  where  t05.T06_codeinterne_i=? and t05.T01_codeinterne_i=? and t05.T05_parent_piece_i=?";
	
?>