<?php
	$page_name="";
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");
	
	$status = $T12->getStatusByPieceID($_GET['id'], $_SESSION['user']['id_user']);

	if($status[0]->T10_code_status_va == "FACTURE" || $status[0]->T10_code_status_va == "SOLDE")
	{
		$BROADCAST = "Ce devis est lié à une facture, il ne peut pas être supprimé";
	}
	else
	{
		$BROADCAST = "Devis supprimé";
		$T05->deleteDevis($_GET['id'], $_SESSION['user']['id_user']);
		$T12->delete($_GET['id']);
	}
	
	$templateFile = "entreprise_ventes_devis_supprimer.html";
	$tmpVars = array("/{CONTENT}/i");
	$replace = array($content);	
	$dynamic_vars = array();
	
?>