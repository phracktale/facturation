<?php
	$page_name="";
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");
	$type_piece = 2;
	
	$pieces = $T05->getPiecesByUser($type_piece, $_SESSION['user']['id_user']);
	
	if(count($pieces) > 0 )
	{
		$liste_pieces = '<table class="datatable_pieces"><thead><tr><th>Date</th><th>Num devis</th><th >Client</th><th>Libellé</th><th >Total HT</th><th >Total facturé</th><th>Status</th><th class="w50p"><th class="w50p"></th><th class="w50p"></th><th class="w50p"></th></tr></thead><tbody>';
		foreach($pieces as $piece)
		{
			if($piece->T03_raison_sociale_va != "")
			{
				$client_libelle = $piece->T03_raison_sociale_va;
			}
			else
			{
				$client_libelle = $piece->T03_contact_nom_va . " " . $piece->T03_contact_prenom_va;
			}
			
			$status = $T12->getStatusByNumPiece($piece->T05_numPiece_va, $_SESSION['user']['id_user']);
			
			$facture = $T05->getSumMontantFacturesFromDevis(1, $_SESSION['user']['id_user'], $piece->T05_codeinterne_i);
	
			$liste_pieces .= '<tr><td>' . $piece->T05_datePiece_d . '</td><td>' . $piece->T05_numPiece_va . '</td><td>' . $client_libelle . '</td><td>' . $piece->T05_intitule_va . '</td><td class="txtright">' . sprintf("%02.2f", $piece->T05_totalHT_n) . '</td><td class="txtright">' . sprintf("%02.2f", $facture[0]->total_acompte) . '</td><td class="txtright">' . $status[0]->etat . '</td><td><input type="image" src="templates/images/icon_pdf.png" rel="devis|' . $piece->T05_numPiece_va . '|pdf" class="bouton export_piece" value="Export PDF" /></td><td><a href="?page=devis_editer&id=' . $piece->T05_codeinterne_i . '" title="Editer"><img src="templates/images/icon_editer.png" alt="Editer"  width="16" height="16"/></a></td><td><a href="?page=devis_dupliquer&id=' . $piece->T05_codeinterne_i . '" title="Dupliquer"><img src="templates/images/icon_dupliquer.png" alt="Dupliquer"  width="16" height="16"/></a></td><td><a href="?page=devis_supprimer&id=' . $piece->T05_codeinterne_i . '" title="Supprimer le devis"><img src="templates/images/icon_supprimer.png" alt="Supprimer le devis" width="16" height="16"/></a></td></tr>';
		}
		$liste_pieces .= '</tbody></table>';
	}
	
	
	$templateFile = "entreprise_ventes_devis_liste.html";
	$tmpVars = array("/{CONTENT}/i", "/{LISTE_DEVIS}/i");
	$replace = array($content, $liste_pieces);	
	$dynamic_vars = array();
	
?>