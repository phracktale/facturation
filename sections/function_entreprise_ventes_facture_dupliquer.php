<?php
	$page_name="";
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");
	$type_piece = 1;
	
	$piece = $T05->getPieceByID($type_piece, $_SESSION['user']['id_user'], $_GET['id']);
	if(count($piece)>0)
	{
		$codeinterne = $T05->insert(fr2mysqlDate(now('date')), "", $_SESSION['user']['id_user'], 0, $piece[0]->T06_codeinterne_i, $piece[0]->T03_codeinterne_i, $piece[0]->T05_dateEcheance_d, $piece[0]->T05_intitule_va,  $piece[0]->T10_code_modeReglement_va,  $piece[0]->T05_remiseGlobale_n,  $piece[0]->T05_totalHT_n,  $piece[0]->T05_totalTVA_n,  $piece[0]->T05_totalTTC_n, $piece[0]->T05_lignes_va);
		
		if($codeinterne > 0)
		{
			$BROADCAST = "Votre facture a été dupliquée elle est sauvegardée en mode brouillon";
		}
		else
		{
			$BROADCAST = "Votre facture n'a pas été dupliquée il y a eu un problème, merci de recommencer l'opération";
		}
	}
	else
	{
		$BROADCAST = "Cette facture n'existe pas qu'essayez vous de faire ?";
	}
	
	$templateFile = "entreprise_ventes_facture_dupliquer.html";
	$tmpVars = array("/{CONTENT}/i");
	$replace = array($content);	
	$dynamic_vars = array();
	
?>