<?php
	$page_name="";
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");
	$type_piece= 1; // Facture
	
	include('actions/piecesOptions.php');
	
	if(count($facture) > 0)
	{
		/* Affichage des contrôles selon le contexte */
		if($facture[0]->T05_numPiece_va == "")
		{
			$boutons_sauve = '<li class="col pr2"><label>&nbsp;</label><input type="button" class="bouton bouton_orange" value="Sauver facture" rel="facture" id="save" /></li><li class="col pr2"><label>&nbsp;</label><input type="button" class="bouton bouton_orange" value="Valider facture" rel="facture" id="valider" /></li>';
			
			$bouton_edite = '<ul class="table">
			<li class="col pr2"><label>&nbsp;</label><input type="button" class="bouton" value="Nouveau titre" id="new_title" /></li><li class="col pr2"><label>&nbsp;</label><input type="button" class="bouton" value="Nouveau commentaire" id="new_com" /></li></ul>';
		}
		else
		{
			$boutons_sauve = "";
			$bouton_edite = "";
			$liste_services = "";
			$liste_produits = "";
		}
		
		/* Affichage des lignes constituant la piece */
		
		$n_line = 0;
		if(count($facture) > 0)
		{
			$lignes_json = json_decode($facture[0]->T05_lignes_va);
			foreach ($lignes_json as $ligne)
			{
				$n_line++;
				switch ($ligne->type)
				{
					default:
					case "line":
						$option1 = $ligne->tva == 0.2 ? 'selected="selected"' : '';
						$option2 = $ligne->tva == 0.196 ? 'selected="selected"' : '';
						$option3 = $ligne->tva == 0.085 ? 'selected="selected"' : '';
						$option4 = $ligne->tva == 0.07 ? 'selected="selected"' : '';
						$option5 = $ligne->tva == 0.055 ? 'selected="selected"' : '';
						$option6 = $ligne->tva == 0.021 ? 'selected="selected"' : '';
						$option7 = $ligne->tva == 0 ? 'selected="selected"' : '';
						
						$lignes .= '<div rel="ligne" id="line_' . $n_line . '" class="line line_' . $n_line . '"><a href="#" class="grip"></a><ul class="table"><li class="col w500p"><textarea class="libelle w100" name="libelle_' . $n_line . '"  >' . $ligne->libelle . '</textarea></li><li class="col w50p"><input type="text" class="quantite w100 txtright" name="quantite_' . $n_line . '" value="' . round($ligne->quantite,1) . '"/></li><li class="col w100p"><select  class="tva w100" name="tva_' . $n_line . '" ><option value="0.2" ' . $option1 . '>20%</option><option value="0.196" ' . $option2 . '>19.6%</option><option value="0.085"' . $option3 . '>8.5%</option><option value="0.07"' . $option4 . '>7%</option><option value="0.055"' . $option5 . '>5.5%</option><option value="0.021"' . $option6 . '>2.1%</option><option value="0"' . $option7 . '>0%</option></select></li><li class="col w100p"><div class="mini-tool"><a href="#"  class="calculette_ttc">TTC</a></div><input type="text" class="puht w100 txtright" name="puht_' . $n_line . '"  value="' . round($ligne->puht,4) . '"/></li><li class="col w100p"><input type="text" class="pht w100 txtright" name="pht_' . $n_line . '" value="' . round($ligne->pht,4) . '" readonly/></li><li class="col line_suppr"><a class="supprime" href="#" title="supprimer ligne"><img src="templates/images/icon_delete.png" alt="Supprimer ligne" width="24" height="24"/></a></li></ul></div>';
						break;
						
					case "titre":
						$lignes .= '<div rel="titre" id="line_' . $n_line . '" class="line line_' . $n_line . '"><a href="#" class="grip"></a><ul class="table w100"><li class="col"><input type="text" class="titre w100" name="titre_' . $n_line . '"  value="' . $ligne->libelle . '"/></li><li class="col line_suppr"><a class="supprime" href="#" title="supprimer ligne"><img src="templates/images/icon_delete.png" alt="Supprimer ligne" width="24" height="24"/></a></li></ul></div>';
						break;
						
					case "commentaire":
						$lignes .= '<div rel="commentaire" id="line_' . $n_line . '" class="line line_' . $n_line . '"><a href="#" class="grip"></a><ul class="table w100"><li class="col"><textarea  class="commentaire w100" name="commentaire_' . $n_line . '">' . $ligne->libelle . '</textarea></li><li class="col line_suppr"><a class="supprime" href="#" title="supprimer ligne"><img src="templates/images/icon_delete.png" alt="Supprimer ligne" width="24" height="24"/></a></li></ul></div>';
						break;
				}
			}
		}
		
		$date_echeance = $facture[0]->T05_dateEcheance_d == "0000-00-00" ? "" : mysql2FrDate($facture[0]->T05_dateEcheance_d);
		
		$templateFile = "entreprise_ventes_facture_editer.html";
		$tmpVars = array("/{LISTE_CLIENTS}/i", "/{LISTE_SERVICES}/i", "/{LISTE_PRODUITS}/i" , "/{LISTE_TVA}/i", "/{LISTE_MODEREGLEMENT}/i", "/{LIGNES}/i", "/{N_LINE}/i", "/{NUM_FACTURE}/i", "/{DATE_FACTURE}/i", "/{DATE_ECHEANCE}/i", "/{INTITULE}/i", "/{REMISE}/i", "/{CODE_INTERNE}/i", "/{BOUTONS_SAUVE}/i", "/{BOUTONS_EDITE}/i");
		$replace = array($liste_clients, $liste_services, $liste_produits, $liste_tauxtva, $liste_modeReglement, $lignes, $n_line, $facture[0]->T05_numPiece_va, mysql2FrDate($facture[0]->T05_datePiece_d), $date_echeance, $facture[0]->T05_intitule_va, $facture[0]->T05_remiseGlobale_n, $facture[0]->T05_codeinterne_i, $boutons_sauve, $bouton_edite);	
		$dynamic_vars = array();
	}
	else
	{
		header('location:index.php?page=facture_liste');
	}
?>