<?php
	$page_name="";
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");
	
	$status = $T12->getStatusByPieceID($_GET['id'], $_SESSION['user']['id_user']);
	
	
	if($status[0]->T10_code_status_va == "VALIDE" )
	{
		$BROADCAST = "Cet avoir est lié à une facture et validé, il ne peut pas être supprimé";
	}
	else
	{
		$BROADCAST = "rien";
		$avoir = $T05->getPieceByID(3, $_SESSION['user']['id_user'], $_GET['id']);
		if(count($avoir) > 0)
		{
			$T12->update("VALIDE", $avoir[0]->T05_parent_piece_i);
			$T05->deleteAvoir($_GET['id'], $_SESSION['user']['id_user']);
			$T12->delete($_GET['id']);
			$BROADCAST = "avoir supprimé";
		}
		
	}
	
	
	$templateFile = "entreprise_ventes_avoir_supprimer.html";
	$tmpVars = array("/{CONTENT}/i");
	$replace = array($content);	
	$dynamic_vars = array();
	
?>