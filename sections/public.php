<?php
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	## Profil public
	#
	#	Routage des fonctionnalités du role public
	#
	#	Attention ! Ce fichier ne doit jamais être édité
	
	$soustitre = "";
	
	switch($_SESSION["contexte"]["rubrique"])
	{
		default:
		case "accueil":
			$titre = "Accueil";
			include("public_accueil.php");
			break;
			
	}
	if(isset($_GET["popin"])){echo $page->output();exit;}else{$html = $page->output();}	
	
	$templateDir = "templates/";
	$templateFile = "main_public.html"; 
	$tmpVars = array("/{CONTENT}/i", "/{MENU_PRINCIPAL}/i", "/{TITRE}/i", "/{SOUSTITRE}/i", "/{INFOS_CONNEXION}/i", "/{REDIRECT}/i","/{META_TITLE}/i", "/{META_KEYWORDS}/i", "/{META_DESCRIPTION}/i");
	$replace = array($html, $menu_principal, $titre, $soustitre, infos_connexion(), $_SERVER["REQUEST_URI"], $meta_title, $meta_keywords, $meta_description);
	$page = new Page($templateDir, $templateFile, $_SESSION["locale"]);
	$page->replace_tags($tmpVars, $replace);
	echo $page->output();
?>