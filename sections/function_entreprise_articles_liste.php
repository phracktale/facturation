<?php
	$page_name="";
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");
	
	$articles = $T08->getArticlesByUser($_SESSION['user']['id_user']);
	
	if(count($articles) > 0 )
	{
		$liste_articles = '<table  class="datatable"><thead><tr><th class="w100p">Référence</th><th class="w300p">Article</th><th class="w100p" >libellé</th><th  class="w100p">Taux TVA</th><th  class="w100p">Prix HT</th><th class="w50p"></th></tr></thead><tbody>';
		foreach($articles as $article)
		{
			$liste_articles .= '<tr><td>' . $article->T08_reference_va . '</td><td>' . $article->T08_libelle_va . '</td><td>' . $article->T10_libelle_va . '</td><td>' . $article->T09_taux_d . '</td><td>' . $article->T08_prixht_d . '</td><td><a href="?page=editer&id=' . $article->T08_codeinterne_i . '" title="Editer la fiche client"><img src="templates/images/icon_editer.png" alt="Editer la fiche client"  width="16" height="16"/></a></td></tr>';
		}
		$liste_articles .= '</tbody></table>';
	}
	
	$templateFile = "entreprise_articles_liste.html";
	$tmpVars = array("/{CONTENT}/i", "/{LISTE_ARTICLES}/i");
	$replace = array($content, $liste_articles);	
	$dynamic_vars = array();
	
?>