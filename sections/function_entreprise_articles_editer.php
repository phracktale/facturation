<?php
	$page_name="";
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");

	$form = new Formulaire($conn);
	$form->load("formulaires/articles");
	$form->setRedirection("?page=liste");
	$form->setStaticInput('T01_codeinterne_i', $_SESSION['user']['id_user']);
	
	if(isset($_POST['submit']))
	{
		$retour = $form->update($_GET['id']);
		$error = $retour['error'];
		
		// test erreur => envoi email de confirmation
	}
	
	$article = $form->get($_GET['id']);
	
	$tmpVars = array();
	$replace = array();
	
	$templateVars = $form->getTemplateFormVars($article, $error, "form");
	$tmpVars = $templateVars['tmpVars'];
	$replace = $templateVars['replace'];
	
	$templateFile = "entreprise_articles_editer.html";
?>