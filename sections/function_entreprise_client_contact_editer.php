<?php
	$page_name="";
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");

	$form = new Formulaire($conn);
	$form->load("formulaires/clients.xml");
	$form->setRedirection("?page=clients_liste");
	
	if(isset($_POST['submit']))
	{
		$retour = $form->save($_GET['id']);
		$error = $retour['error'];
		
		// test erreur => envoi email de confirmation
	}

	
	$client = $form->get($_GET['id']);

	
	$tmpVars = array();
	$replace = array();
	
	$templateVars = $form->getTemplateFormVars($client, $error, "form");
	$tmpVars = $templateVars['tmpVars'];
	$replace = $templateVars['replace'];
	
	$templateFile = "entreprise_client_contact_editer.html";

	
?>