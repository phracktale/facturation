<?php
	$page_name="";
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");
	
	$clients = $T03->getCustomerByUser($_SESSION['user']['id_user']);
	
	if(count($clients) > 0 )
	{
		$liste_clients = '<table  class="datatable"><thead><tr><th class="w100p">Num fact</th><th class="w150p">Raison sociale</th><th>Contact</th><th class="w100p">Localité</th><th class="w50p"></th><th class="w50p"></th><th class="w50p"></th></tr></thead><tbody>';
		foreach($clients as $client)
		{
			$liste_clients .= '<tr><td>' . $client->T03_codeinterne_i . '</td><td>' . $client->T03_raison_sociale_va . '</td><td>' . $client->T04_civilite_va . ' ' . $client->T04_nom_va . ' ' . $client->T04_prenom_va . '</td><td class="txtright">' . $client->T03_ville_va . '</td><td><a href="?page=client_editer&id=' . $client->T03_codeinterne_i . '" title="Editer la fiche client"><img src="templates/images/icon_editer.png" alt="Editer la fiche client"  width="16" height="16"/></a></td></tr>';
		}
		$liste_clients .= '</tbody></table>';
	}
	
	$templateFile = "entreprise_client_contact_liste.html";
	$tmpVars = array("/{CONTENT}/i", "/{LISTE_CLIENTS}/i");
	$replace = array($content, $liste_clients);	
	$dynamic_vars = array();
	
?>