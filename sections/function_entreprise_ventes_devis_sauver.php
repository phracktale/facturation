<?php
	$page_name="";
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");
	$type_piece= 2; // Devis
	$entete = $_POST['entete'];
	$lines = json_encode($_POST['lines']);

	if($entete['remise_globale'] == "")
	{
		$entete['remise_globale'] = 0;
	}
	
	/*
	print_r($_POST['entete']);
	print_r($_POST['lines']);
	*/
	
	$piece = $T05->getDevisByID($type_piece, $_SESSION['user']['id_user'], $entete['code_interne']);
	
	if($entete['mode_sauvegarde'] == "transfert_facture" || $entete['mode_sauvegarde'] == "transfert_acompte")
	{
		$facture = $T05->getNewNumOrder($_SESSION['user']['id_user'], 1);
		$numero_piece = "FA" . sprintf("%06d", $facture[0]->num_piece);
		
		if($entete['mode_sauvegarde'] == "transfert_facture")
		{
			/* 	Dans le cas de la facturation du solde 
				trouver les acomptes éventuels
				et reconstituer les lignes de la facture finale 
			*/
			$acomptes = $T05->getPiecesFromParent(1, $_SESSION['user']['id_user'], $entete['code_interne']);
			if(count($acomptes) > 0)
			{
				$lines = (array) json_decode($lines);
				
				$total_acompte = 0;
				foreach($acomptes as $acompte)
				{
					$total_acompte += $acompte->T05_totalHT_n;
					$total_tva += $acompte->T05_totalHT_n * $acompte->T05_tva_n;
					$total_ttc += $acompte->T05_totalHT_n + $acompte->T05_totalHT_n * $acompte->T05_totalTVA_n;
					$taux_tva =  (array)json_decode($acompte->T05_lignes_va);
					
					// Il n'y a qu'une ligne dans une facture d'acompte 
					foreach ($taux_tva as $item_tva)
					{
						$taux_tva =  $item_tva->tva;
					}
					
					$lines[count($lines)+1] = array(
										'type' => 'ligne',
										'quantite' => 1,
										'libelle' => "Facture d'acompte N°" .  $acompte->T05_numPiece_va . " du " . mysql2FrDate($acompte->T05_datePiece_d),
										'tva' => $taux_tva,
										'puht' => -$acompte->T05_totalHT_n,
										'pht' => -$acompte->T05_totalHT_n
					);
				}
				$lines =json_encode($lines);
			}
			
			$T12->update("SOLDE", $entete['code_interne']);
		}
		else
		{
			$T12->update("FACTURE", $entete['code_interne']);
		}
		
		if($entete['date_echeance'] == "")
		{
			$entete['date_echeance'] = date('d/m/Y', strtotime("+1 month"));
		}

		$codeinterne = $T05->insert(fr2mysqlDate(now('date')), "", $_SESSION['user']['id_user'], $entete['code_interne'], 1, $entete['client_id'], fr2mysqlDate($entete['date_echeance']), $entete['intitule'], $entete['moyenReglement'], $entete['remise_globale'], $entete['total_ht'] - $total_acompte, $entete['total_tva'] - $total_tva, $entete['total_ttc'] - $total_ttc, $lines);


		$T12->insert($codeinterne);
		
		$T05->valider($numero_piece, $_SESSION['user']['id_user'], 1, $entete['client_id'], fr2mysqlDate($entete['date_echeance']), $entete['intitule'], $entete['moyenReglement'], $entete['remise_globale'], $entete['total_ht'] - $total_acompte, $entete['total_tva'] - $total_tva, $entete['total_ttc'] - $total_ttc, $lines, $codeinterne);

		$T12->update("VALIDE", $codeinterne);
		
		$oRetour['code_interne'] = $entete['code_interne'];
		$oRetour['entete_total_ttc'] = $entete['total_ttc'];
		$oRetour['total_ttc'] = $total_ttc;
		$oRetour['tva'] = $total_tva;
	}
	elseif($entete['mode_sauvegarde'] == "brouillon")
	{
		if(count($piece) > 0 )
		{	
			$T05->update($_SESSION['user']['id_user'], $type_piece, $entete['client_id'], fr2mysqlDate($entete['date_echeance']), $entete['intitule'], $entete['moyenReglement'], $entete['remise_globale'], $entete['total_ht'], $entete['total_tva'], $entete['total_ttc'], $lines, $entete['code_interne']);
			$T12->update("A_FACTURER", $entete['code_interne']);
			
			$oRetour['numero_piece'] = $entete['numero_piece'];
			$oRetour['code_interne'] = $entete['code_interne'];
		}
	}
	
	
		
	echo json_encode($oRetour);
	exit;
?>