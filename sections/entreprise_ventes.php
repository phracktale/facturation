<?php
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	

	switch($_SESSION["contexte"][$_SESSION["contexte"]["rubrique"]]["page"])
	{
		default:
		
		case "accueil":
			include("function_entreprise_ventes_accueil.php");
			break;
		case "facture_liste":
			include("function_entreprise_ventes_facture_liste.php");
			break;
		case "facture_nouveau":
			include("function_entreprise_ventes_facture_nouveau.php");
			break;
		case "facture_editer":
			include("function_entreprise_ventes_facture_editer.php");
			break;
		case "facture_sauver":
			include("function_entreprise_ventes_facture_sauver.php");
			break;
		case "facture_transferer":
			include("function_entreprise_ventes_facture_transferer.php");
			break;
		case "facture_supprimer":
			include("function_entreprise_ventes_facture_supprimer.php");
			break;
		case "facture_dupliquer":
			include("function_entreprise_ventes_facture_dupliquer.php");
			break;
		case "facture_export":
			include("function_entreprise_ventes_facture_export.php");
			break;
		case "devis_liste":
			include("function_entreprise_ventes_devis_liste.php");
			break;
		case "devis_nouveau":
			include("function_entreprise_ventes_devis_nouveau.php");
			break;
		case "devis_editer":
			include("function_entreprise_ventes_devis_editer.php");
			break;
		case "devis_sauver":
			include("function_entreprise_ventes_devis_sauver.php");
			break;
		case "devis_transferer":
			include("function_entreprise_ventes_devis_transferer.php");
			break;
		case "devis_supprimer":
			include("function_entreprise_ventes_devis_supprimer.php");
			break;
		case "devis_dupliquer":
			include("function_entreprise_ventes_devis_dupliquer.php");
			break;
		case "devis_export":
			include("function_entreprise_ventes_devis_export.php");
			break;
		case "avoir_liste":
			include("function_entreprise_ventes_avoir_liste.php");
			break;
		case "avoir_nouveau":
			include("function_entreprise_ventes_avoir_nouveau.php");
			break;
		case "avoir_editer":
			include("function_entreprise_ventes_avoir_editer.php");
			break;
		case "avoir_sauver":
			include("function_entreprise_ventes_avoir_sauver.php");
			break;
		case "avoir_transferer":
			include("function_entreprise_ventes_avoir_transferer.php");
			break;
		case "avoir_supprimer":
			include("function_entreprise_ventes_avoir_supprimer.php");
			break;
		case "avoir_dupliquer":
			include("function_entreprise_ventes_avoir_dupliquer.php");
			break;
		case "avoir_export":
			include("function_entreprise_ventes_avoir_export.php");
			break;
	}
	
	$page = new Page($templateDir, $templateFile,  $_SESSION["locale"]);
	$page->setVars($dynamic_vars);
	
	$page->replace_tags($tmpVars, $replace);
	$html = $page->output();
	
?>