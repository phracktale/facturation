<?php
	$facture = $T05->getPieceByNumOrder($type_piece, $_SESSION['user']['id_user'], $num_piece);
	
	$T05_codeinterne_i = $facture[0]->T05_codeinterne_i;
	$T01_codeinterne_i = $facture[0]->T01_codeinterne_i;
	$T05_parent_piece_i = $facture[0]->T05_parent_piece_i;
	$T06_codeinterne_i = $facture[0]->T06_codeinterne_i;
	$T03_codeinterne_i = $facture[0]->T03_codeinterne_i;
	$T05_numPiece_va = $facture[0]->T05_numPiece_va;
	$T05_datePiece_d = $facture[0]->T05_datePiece_d;
	$T05_dateEcheance_d	 = $facture[0]->T05_dateEcheance_d;
	$T05_intitule_va = $facture[0]->T05_intitule_va;
	$T10_code_modeReglement_va = $facture[0]->T10_code_modeReglement_va;
	$T05_lignes_va = $facture[0]->T05_lignes_va;
	$T05_remiseGlobale_n = $facture[0]->T05_remiseGlobale_n;

	/* Rechercher les tva de chaque taux en parsant le champs T05_lignes_va */
	$lignes = (array) json_decode($T05_lignes_va);
	foreach($lignes as $ligne)
	{
		if($ligne->type == "ligne")
		{
			$pht = $ligne->pht - $ligne->pht * $T05_remiseGlobale_n / 100;
			$key_tva = str_replace('.' , '' , $ligne->tva*100);
			$liste_tva[$key_tva]['taux'] = $ligne->tva*100;
			$liste_tva[$key_tva]['valeur'] = $liste_tva[$key_tva]['valeur'] + $ligne->tva * $pht;
			$T05_totalHT_n += $ligne->pht;
		}
	}
	
	$total_HT_remise = $T05_totalHT_n * $T05_remiseGlobale_n / 100;
	
	foreach($liste_tva as $tva)
	{
		$total_tva += $tva['valeur'];
	}
	$T05_totalTTC_n = $T05_totalHT_n - $total_HT_remise + $total_tva ;
	
	
	$T03_raison_sociale_va = $facture[0]->T03_raison_sociale_va;
	$T03_service_va = $facture[0]->T03_service_va;
	$T03_siret_va = $facture[0]->T03_siret_va;
	$T03_adresse1_va = $facture[0]->T03_adresse1_va;
	$T03_adresse2_va = $facture[0]->T03_adresse2_va;
	$T03_codepostal_va = $facture[0]->T03_codepostal_va;
	$T03_ville_va = $facture[0]->T03_ville_va;
	$T03_pays_va = $facture[0]->T03_pays_va;
	$T03_site_web_va = $facture[0]->T03_site_web_va;
	$T03_telephone_va = $facture[0]->T03_telephone_va;
	$T03_num_tva_va = $facture[0]->T03_num_tva_va;
	
	$T03_responsable_va = $facture[0]->T03_responsable_va;
	$T10_contact_code_civilite_va = $facture[0]->T10_contact_code_civilite_va;
	$T03_contact_nom_va = $facture[0]->T03_contact_nom_va;
	$T03_contact_prenom_va = $facture[0]->T03_contact_prenom_va;
	$T03_contact_fonction_va = $facture[0]->T03_contact_fonction_va;
	$T03_contact_telephone_va = $facture[0]->T03_contact_telephone_va;
	$T03_contact_email_va = $facture[0]->T03_contact_email_va;
	

	
	
	
	$my = $T01->selectByID($_SESSION['user']['id_user']);
	
	$T01_raison_sociale_va = $my->T01_raison_sociale_va;
	$T01_siret_va = $my->T01_siret_va;
	$T01_rcs_va = $my->T01_rcs_va;
	$T01_num_tva_va = $my->T01_num_tva_va;
	$T01_ape_va = $my->T01_ape_va;
	$T01_capital_va = $my->T01_capital_va;
	$T01_adresse1_va = $my->T01_adresse1_va;
	$T01_adresse2_va = $my->T01_adresse2_va;
	$T01_codepostal_va = $my->T01_codepostal_va;
	$T01_ville_va = $my->T01_ville_va;
	$T01_email_va = $my->T01_email_va;
	$T01_telephone_va = $my->T01_telephone_va;
	$T01_portable_va = $my->T01_portable_va;
	
	$coord_entreprise = $T01_adresse1_va . "\n";
	if($T01_adresse2_va != "" ){$coord_entreprise .= $T01_adresse2_va . "\n";}
	$coord_entreprise .= $T01_codepostal_va . " " . $T01_ville_va . "\n";
	if($T01_email_va != "" ){$coord_entreprise .= "Email: " . $T01_email_va . "\n";}
	if($T01_telephone_va != "" ){$coord_entreprise .= "Tél: " . $T01_telephone_va . "\n";}
	if($T01_portable_va != "" ){$coord_entreprise .= "Portable: " . $T01_telephone_va . "\n";}
?>