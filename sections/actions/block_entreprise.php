<?php
	if(is_file($_SESSION['user']['logo']))
	{
		$PDF->Image($_SESSION['user']['logo'], 146, 10, 53, 33);
	}
	
	$PDF->SetFont('DejaVu_bold','',14);
	$PDF->setXY(6.8, 20);
	$PDF->Cell(82, 3, $T01_raison_sociale_va );
	
	$PDF->SetFont('DejaVu','',10);
	$PDF->setXY(6.8, 26);
	$PDF->MultiCell(82, 4, $coord_entreprise);
	
	
	/* entete facture */
	$PDF->setXY(9.2, 104);
	$PDF->Cell(25, 4, $num_piece);
	
	$PDF->setXY(37.2, 104);
	$PDF->Cell(25, 4, $T05_datePiece_d);
	
	$PDF->setXY(61.1, 104);
	$PDF->Cell(25, 4, $T03_codeinterne_i);
	
	$PDF->setXY(85.2, 104);
	$PDF->Cell(25, 4, $T05_dateEcheance_d);
	
	$PDF->setXY(119.3, 104);
	$PDF->Cell(50, 4, $mode_reglement);
	
	$PDF->setXY(172.7, 104);
	$PDF->Cell(37, 4, $T01_num_tva_va);
	
	/* footer */
	$info_footer = "Siret : " . $T01_siret_va;
	if($T01_ape_va != '' ) {$info_footer .= " - APE : " . $T01_ape_va;}
	if($T01_rcs_va != '' ) {$info_footer .= " - RCS : " . $T01_rcs_va;}
	if($T01_num_tva_va != '' ) {$info_footer .= " - No TVA intracom : " . $T01_num_tva_va;}
	if($T01_capital_va != '' ) {$info_footer .= " - Capital : " . $T01_capital_va;}
	
	$PDF->footer($info_footer);
	
?>