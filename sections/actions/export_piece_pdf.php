<?php
	
	require('classes/fpdf/fpdf.php');
	require('classes/fpdf/fpdi.php');
	require('classes/fpdf/MyFPDF.php');
	$PDF =& new MyFPDF();
	$PDF->AliasNbPages();	
	$PDF->SetAutoPageBreak(false);
	$PDF->AddPage(); 
	$PDF->setSourceFile($piece_file); 
	$tpl_facture = $PDF->importPage(1);
	
	
	$PDF->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
	$PDF->AddFont('DejaVu_bold','','DejaVuSansCondensed-Bold.ttf',true);
	
	$PDF->SetTextColor(0, 0, 0);
	$PDF->useTemplate($tpl_facture, 0, 0, 210); 
	
	include('sections/actions/block_entreprise.php');
	include('sections/actions/block_client.php');
	$Y = 120.4;
	$line = 0;
	$X_reference = 9;
	$X_libelle = 30.9;
	$X_quantite = 124;
	$X_puht = 147.6;
	$X_pht = 169.4;
	$X_tva = 192;
	
	$H_line = 4.5;
	
	$L_reference = 18.5;
	$L_libelle = 90;
	$L_quantite = 21.3;
	$L_puht = 19.4;
	$L_pht = 19.5;
	$L_tva = 11.2;
	
	
	
	/* Gestion des lignes de facturation  */
	foreach($lignes as $ligne)
	{
		if($line > 20)
		{
			$line = 0;
			$PDF->AddPage(); 
			$PDF->setSourceFile($piece_file); 
			$tpl_facture = $PDF->importPage(1);
			$PDF->useTemplate($tpl_facture, 0, 0, 210); 
			$PDF->SetTextColor(0,0,0);
			include('sections/actions/block_entreprise.php');
			include('sections/actions/block_client.php');
			
		}
		
		if($ligne->type == "ligne")
		{
			/*
				$PDF->setXY($X_reference, $Y * $line * $H_line);
				$PDF->Cell($L_reference, $H_line, $ligne->reference, 0, 0, 'R');
			*/
			$PDF->SetTextColor(60,60,60);
			$PDF->SetFont('DejaVu','',9);
			$PDF->setXY($X_libelle, $Y + $line * $H_line);
			$PDF->MultiCell($L_libelle, $H_line, $ligne->libelle);
			
			$PDF->setXY($X_quantite, $Y + $line * $H_line);
			$PDF->Cell($L_quantite, $H_line, number_format($ligne->quantite, 1, '.', ' '), 0, 0, 'R');
			
			$PDF->setXY($X_puht, $Y + $line * $H_line);
			$PDF->Cell($L_puht, $H_line, number_format($ligne->puht, 2, '.', ' ') . " €", 0, 0, 'R');
			
			$PDF->setXY($X_pht, $Y + $line * $H_line);
			$PDF->Cell($L_pht, $H_line, number_format($ligne->pht, 2, '.', ' ') . " €", 0, 0, 'R');
			
			$PDF->setXY($X_tva, $Y + $line * $H_line);
			$PDF->Cell($L_tva, $H_line, number_format($ligne->tva*100, 2, '.', ' ') . " %", 0, 0, 'R');

		}
		elseif($ligne->type == "titre")
		{
			
			$PDF->SetTextColor(0,0,0);
			$PDF->SetFont('DejaVu_bold','',12);
			$PDF->setXY($X_libelle, $Y + $line * $H_line);
			$PDF->MultiCell($L_libelle, $H_line, $ligne->libelle);
			$PDF->ln;
			$line += $PDF->nbLines($L_libelle, $ligne->libelle);
			
		}
		elseif($ligne->type == "commentaire")
		{
			$PDF->SetTextColor(120,120,120);
			$PDF->SetFont('DejaVu','', 8);
			$PDF->setXY($X_libelle, $Y + $line * $H_line);
			$PDF->MultiCell($L_libelle, $H_line, $ligne->libelle);
			$PDF->ln;
			$line += $PDF->nbLines($L_libelle, $ligne->libelle);
		
		}
		
		$line += $PDF->nbLines($L_libelle, $ligne->libelle);
			
		
	}
	
	
	/* Gestion du bloc total */
	
	$Y = 250;
	$X_data = 167;
	$L_data = 36;
	$X_label = 123;
	$L_label = 44.1;
	$line = 0;
	$H_line = 6.1;
	$PDF->SetFillColor(222, 222, 222);
	$PDF->SetDrawColor (222, 222, 222);
	$PDF->SetFont('DejaVu','',11);
	
	if($T05_remiseGlobale_n > 0)
	{
		$PDF->setXY($X_label, $Y + $line * $H_line);
		$PDF->Cell($L_label, $H_line, "sous-total HT" ,1, 0, 'R', true);
		$PDF->setXY($X_data, $Y + $line * $H_line);
		$PDF->Cell($L_data, $H_line, number_format($T05_totalHT_n, 2, '.', ' ') . " €",1, 0, 'R', false);
		$line++;
		
		$PDF->setXY($X_label, $Y + $line * $H_line);
		$PDF->Cell($L_label, $H_line, "Remise (" . $T05_remiseGlobale_n . "%)" ,1, 0, 'R', true);
		$PDF->setXY($X_data, $Y + $line * $H_line);
		$PDF->Cell($L_data, $H_line, number_format($total_HT_remise, 2, '.', ' ') . " €", 1, 0, 'R', false);
		$line++;
		
		$PDF->setXY($X_label, $Y + $line * $H_line);
		$PDF->Cell($L_label, $H_line, "total HT" ,1, 0, 'R', true);
		$PDF->setXY($X_data, $Y + $line * $H_line);
		$PDF->Cell($L_data, $H_line, number_format($T05_totalHT_n - $total_HT_remise, 2, '.', ' ') . " €",1, 0, 'R', false);
		$line++;
	}
	else
	{
		$PDF->setXY($X_label, $Y + $line * $H_line);
		$PDF->Cell($L_label, $H_line, "total HT" ,1, 0, 'R', true);
		$PDF->setXY($X_data, $Y + $line * $H_line);
		$PDF->Cell($L_data, $H_line, number_format($T05_totalHT_n, 2, '.', ' ') . " €",1, 0, 'R', false);
		$line++;
	}
	
	foreach($liste_tva as $tva)
	{
		
		$PDF->setXY($X_label, $Y + $line * $H_line);
		$PDF->Cell($L_label, $H_line, "TVA (" . $tva['taux'] . "%)" ,1, 0, 'R', true);
		$PDF->setXY($X_data, $Y + $line * $H_line);
		$PDF->Cell($L_data, $H_line, number_format($tva['valeur'], 2, '.', ' ') . " €", 1, 0, 'R', false);
		$line++;
	}
	
	$PDF->setXY($X_label, $Y + $line * $H_line);
	$PDF->Cell($L_label, $H_line, "total TTC" ,1, 0, 'R', true);
	$PDF->setXY($X_data, $Y + $line * $H_line);
	$PDF->Cell($L_data, $H_line, number_format($T05_totalTTC_n, 2, '.', ' ') . " €", 1, 0, 'R', false);
	$line++;
	

	// Conditions générales de vente
	$PDF->AddPage(); 
	$PDF->setSourceFile($_SESSION['user']['cgv']); 
	$tpl_facture = $PDF->importPage(1);
	$PDF->useTemplate($tpl_facture, 0, 0, 210); 
			
	$PDF->Output($piece_name . '.pdf', 'D');

?>