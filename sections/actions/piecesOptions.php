<?php

	$facture = $T05->getPieceByID($type_piece, $_SESSION['user']['id_user'], $_GET['id']);
	
	
	$clients = $T03->getCustomerByUser($_SESSION['user']['id_user']);	
	if (count($clients) > 0)
	{
		$liste_clients = "<option></option>";
		foreach($clients as $client)
		{
			if($facture[0]->T03_codeinterne_i == $client->T03_codeinterne_i)
			{
				$isSelected = 'selected="selected"';
			}
			else
			{
				$isSelected = "";
			}
			
			
			$client_libelle = $client->T03_contact_nom_va . " " . $client->T03_contact_prenom_va;
			
			if($client->T03_raison_sociale_va != "")
			{
				$client_libelle = $client->T03_raison_sociale_va;
				if($client->T03_contact_nom_va != "")
				{
					$client_libelle .= " (" . $client_libelle . ")";
				}
		
			}
			$liste_clients .= '<option value="' . $client->T03_codeinterne_i . '" ' . $isSelected . '>' . $client_libelle . '</option>';
		}
	}
		
	$services = $T08->getProducts($_SESSION['user']['id_user'], "SERVICE");
	
	if (count($services) > 0)
	{
		$liste_services = '<ul class="table"><li class="col pr2"><label for="service">Services</label><select name="service"><option></option>';
		foreach($services as $service)
		{
			$liste_services .= '<option value="' . $service->T08_codeinterne_i . '">' . $service->T08_libelle_va . '</option>';
		}
		$liste_services .= '</select></li><li class="col pr2"><label>&nbsp;</label><input type="button"  class="bouton" value="Ajouter service" id="nouveau_service" /></li></ul>';
	}

	$produits = $T08->getProducts($_SESSION['user']['id_user'], "PRODUIT");
	if (count($produits) > 0)
	{
		$liste_produits = '<ul class="table"><li class="col pr2"><label for="produit">Produits</label><select name="produit"><option></option>';
		foreach($produits as $produit)
		{
			$liste_produits .= '<option value="' . $produit->T08_codeinterne_i . '">' . $produit->T08_libelle_va . '</option>';
		}
		$liste_produits .= '</select></li><li class="col pr2"><label>&nbsp;</label><input type="button"  class="bouton" value="Ajouter produit" id="nouveau_produit" /></li></ul>';
	}

	$tauxtva = $T09->selectAll();
	if (count($tauxtva) > 0)
	{
		$liste_tauxtva = "";
		foreach($tauxtva as $tva)
		{
			$liste_tauxtva .= '<option>' . $tva->T09_taux_d . '</option>';
		}
	}
	
	
	$modesReglement = $T10->selectAllByCode("methode_paiement");
	if (count($modesReglement) > 0)
	{
		$liste_modeReglement = "";
		foreach($modesReglement as $modeReglement)
		{
			if($facture[0]->T10_code_modeReglement_va == $modeReglement->T10_code_va)
			{
				$isSelected = 'selected="selected"';
			}
			else
			{
				$isSelected = "";
			}
			
			$liste_modeReglement .= '<option value="' . $modeReglement->T10_code_va . '" ' . $isSelected . '>' . $modeReglement->T10_libelle_va . '</option>';
		}
	}
?>