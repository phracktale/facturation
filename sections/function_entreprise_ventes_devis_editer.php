<?php
	$page_name="";
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");
	$type_piece= 2; // Devis

	include('actions/piecesOptions.php');

	$piece = $T05->getDevisByID($type_piece, $_SESSION['user']['id_user'], $_GET['id']);
	
	$n_line = 0;
	if(count($piece) > 0)
	{
	
		$lignes_json = json_decode($piece[0]->T05_lignes_va);
		foreach ($lignes_json as $ligne)
		{
			$n_line++;
			switch ($ligne->type)
			{
				default:
				case "line":
					$option1 = $ligne->tva == 0.2 ? 'selected="selected"' : '';
					$option2 = $ligne->tva == 0.196 ? 'selected="selected"' : '';
					$option3 = $ligne->tva == 0.085 ? 'selected="selected"' : '';
					$option4 = $ligne->tva == 0.07 ? 'selected="selected"' : '';
					$option5 = $ligne->tva == 0.055 ? 'selected="selected"' : '';
					$option6 = $ligne->tva == 0.021 ? 'selected="selected"' : '';
					$option7 = $ligne->tva == 0 ? 'selected="selected"' : '';
					
					$lignes_edit .= '<div rel="ligne" id="line_' . $n_line . '" class="line line_' . $n_line . '"><a href="#" class="grip"></a><ul class="table"><li class="col w500p"><textarea class="libelle w100" name="libelle_' . $n_line . '"  >' . $ligne->libelle . '</textarea></li><li class="col w50p"><input type="text" class="quantite w100" name="quantite_' . $n_line . '" value="' . $ligne->quantite . '"/></li><li class="col w100p"><select  class="tva w100" name="tva_' . $n_line . '" ><option value="0.2" ' . $option1 . '>20%</option><option value="0.196" ' . $option2 . '>19.6%</option><option value="0.085"' . $option3 . '>8.5%</option><option value="0.07"' . $option4 . '>7%</option><option value="0.055"' . $option5 . '>5.5%</option><option value="0.021"' . $option6 . '>2.1%</option><option value="0"' . $option7 . '>0%</option></select></li><li class="col w100p"><div class="mini-tool"><a href="#" class="calculette_ttc">TTC</a></div><input type="text" class="puht w100" name="puht_' . $n_line . '"  value="' . round($ligne->puht, 4) . '"/></li><li class="col w100p"><input type="text" class="pht w100" name="pht_' . $n_line . '" value="' . round($ligne->pht, 4) . '" readonly/></li><li class="col line_suppr"><a class="supprime" href="#" title="supprimer ligne"><img src="templates/images/icon_delete.png" alt="Supprimer ligne" width="24" height="24"/></a></li></ul></div>';
					
					$lignes .= '<div rel="ligne" class="line"><a href="#" class="grip"></a><ul class="table"><li class="col w500p"><div class="field">' . $ligne->libelle . '</div></li><li class="col w50p"><div class="field">' . $ligne->quantite . '</div></li><li class="col w100p"><div class="field">' . $ligne->tva*100 . '%</div></li><li class="col w100p"><div class="field">' . round($ligne->puht, 4) . '</div></li><li class="col w100p"><div class="field">' . round($ligne->pht, 4) . '</div></li></ul></div>';
					break;
					
				case "titre":
					$lignes_edit .= '<div rel="titre" id="line_' . $n_line . '" class="line line_' . $n_line . '"><a href="#" class="grip"></a><ul class="table w100"><li class="col"><input type="text" class="titre w100" name="titre_' . $n_line . '"  value="' . $ligne->libelle . '"/></li><li class="col line_suppr"><a class="supprime" href="#" title="supprimer ligne"><img src="templates/images/icon_delete.png" alt="Supprimer ligne" width="24" height="24"/></a></li></ul></div>';
					
					$lignes .= '<div class="line"><a href="#" class="grip"></a><ul class="table w100"><li class="col"><div class="field">' . $ligne->libelle . '</div></li></ul></div>';
					break;
					
				case "commentaire":
					$lignes_edit .= '<div rel="commentaire" id="line_' . $n_line . '" class="line line_' . $n_line . '"><a href="#" class="grip"></a><ul class="table w100"><li class="col"><textarea  class="commentaire w100" name="commentaire_' . $n_line . '">' . $ligne->libelle . '</textarea></li><li class="col line_suppr"><a class="supprime" href="#" title="supprimer ligne"><img src="templates/images/icon_delete.png" alt="Supprimer ligne" width="24" height="24"/></a></li></ul></div>';
					
					$lignes .= '<div  class="line "><a href="#" class="grip"></a><ul class="table w100"><li class="col"><div class="field">' . $ligne->libelle . '</div></li></ul></div>';
					break;
			}
		}
	}
	
	$factures = $T05->getPiecesFromParent(1, $_SESSION['user']['id_user'], $_GET['id']);
		
	if(count($factures) > 0)
	{
		/* 
			On cherche si le montant des factures émise pour un devis est plus petit que le montant du devis
			Si non c'est que le devis n'est pas soldé
			en conséquence on affiche le formulaire
		*/
		$totalAcompte = 0;
		$liste_acomptes = '<table class="datatable"><thead><tr><th class="w100p">Date</th><th class="w100p">Num fact</th><th class="w150p">total HT</th></thead><tbody>';
		foreach($factures as $facture)
		{
			$totalAcompte += $facture->T05_totalHT_n;
			$num_piece =  $facture->T05_numPiece_va != "" ? "Facture N°" .  $facture->T05_numPiece_va : "Facture non validée";
			$liste_acomptes .= '<tr><td>' . mysql2FrDate($facture->T05_datePiece_d) . '</td>';
			$liste_acomptes .= '<td>' . $num_piece . '</td>';
			$liste_acomptes .= '<td>' . $facture->T05_totalHT_n . '</td></tr>';
		}
		$liste_acomptes .= '</tbody></table>';
		
		if($totalAcompte < $piece[0]->T05_totalHT_n )
		{
			// Si le montant des acompte est inférieur au total HT du devis
			// Afficher les controles de facturation et de solde
			
			if($totalAcompte > 0 )
			{
				$BROADCAST = "Ce devis n'est pas entièrement soldée";
				$ligne_acompte = '<tr><td>Acompte HT</td><td>' . number_format($totalAcompte, 2, '.', ' ') . '<input type="hidden" name="total_acompte" value="'. $totalAcompte . '" /></td></tr>';
				$ligne_acompte .= '<tr><td>Reste à payer HT</td><td>' . number_format($piece[0]->T05_totalHT_n-$totalAcompte, 2, '.', ' ') . '</td></tr>';
				
			}
			else
			{
				$BROADCAST = "Ce devis est à facturer";
			}
			
			$templateFile = "bouton_facture_acompte.html";
			$page = new Page($templateDir, $templateFile,  $_SESSION["locale"]);
			$bouton_acompte = $page->output();
			
			$templateFile = "bouton_facture_solde.html";
			$page = new Page($templateDir, $templateFile,  $_SESSION["locale"]);
			$bouton_solde = $page->output();
			
			
		}
		else if($totalAcompte == $piece[0]->T05_totalHT_n )
		{
			$BROADCAST = "Ce devis a été facturé";
		}
		else
		{
			$BROADCAST = "Il y a un problème de cohérence dans votre facturation.<br/>Le montant de la facturation correspondant à ce devis ne peut pas être supérieur au montant du devis";
		}
		
		
		$sousTotalHT = $piece[0]->T05_totalHT_n / (1 - $piece[0]->T05_remiseGlobale_n / 100);
		$templateFile = "entreprise_ventes_devis_voir.html";
		$tmpVars = array(  "/{ID_CLIENT}/i", "/{LISTE_ACOMPTES}/i", "/{LIGNES}/i", "/{NUM_DEVIS}/i", "/{DATE_DEVIS}/i", "/{DATE_ECHEANCE}/i", "/{INTITULE}/i", "/{REMISE}/i", "/{CODE_INTERNE}/i", "/{SOUSTOTALHT}/i", "/{TOTALHT}/i", "/{TVA}/i", "/{TOTALTTC}/i", "/{CLIENT}/i", "/{CODE_MODEREGLEMENT}/i" , "/{MODEREGLEMENT}/i" , "/{LIGNE_ACOMPTE}/i" , "/{BOUTON_ACOMPTE}/i" , "/{BOUTON_SOLDE}/i" );
		$replace = array($piece[0]->T03_codeinterne_i, $liste_acomptes, $lignes_edit, $piece[0]->T05_numPiece_va, mysql2FrDate($piece[0]->T05_datePiece_d), mysql2FrDate($piece[0]->T05_dateEcheance_d), $piece[0]->T05_intitule_va, $piece[0]->T05_remiseGlobale_n,  $piece[0]->T05_codeinterne_i, number_format($sousTotalHT, 2, '.', ''),  number_format($piece[0]->T05_totalHT_n, 2, '.', ''), number_format($piece[0]->T05_totalTVA_n, 2, '.', ''), number_format($piece[0]->T05_totalTTC_n, 2, '.', ''), $piece[0]->T03_raison_sociale_va, $piece[0]->T10_code_modeReglement_va, $piece[0]->T10_libelle_va, $ligne_acompte, $bouton_acompte, $bouton_solde);	
		$dynamic_vars = array();
	}
	else
	{
		$BROADCAST = "Ce devis est à facturer";
		
		$templateFile = "bouton_facture_acompte.html";
		$page = new Page($templateDir, $templateFile,  $_SESSION["locale"]);
		$bouton_acompte = $page->output();
		
		$templateFile = "bouton_facture_solde.html";
		$page = new Page($templateDir, $templateFile,  $_SESSION["locale"]);
		$bouton_solde = $page->output();
			
		$templateFile = "entreprise_ventes_devis_editer.html";
		$tmpVars = array( "/{LISTE_ACOMPTES}/i", "/{LISTE_CLIENTS}/i", "/{LISTE_SERVICES}/i", "/{LISTE_PRODUITS}/i" , "/{LISTE_TVA}/i", "/{LISTE_MODEREGLEMENT}/i", "/{LIGNES}/i", "/{N_LINE}/i", "/{NUM_DEVIS}/i", "/{DATE_DEVIS}/i", "/{DATE_ECHEANCE}/i", "/{INTITULE}/i", "/{REMISE}/i", "/{CODE_INTERNE}/i" , "/{BOUTON_ACOMPTE}/i" , "/{BOUTON_SOLDE}/i");
		$replace = array($liste_acomptes, $liste_clients, $liste_services, $liste_produits, $liste_tauxtva, $liste_modeReglement, $lignes_edit, $n_line, $piece[0]->T05_numPiece_va, mysql2FrDate($piece[0]->T05_datePiece_d), mysql2FrDate($piece[0]->T05_dateEcheance_d), $piece[0]->T05_intitule_va, $piece[0]->T05_remiseGlobale_n, $piece[0]->T05_codeinterne_i, $bouton_acompte, $bouton_solde);	
		$dynamic_vars = array();
	}
?>