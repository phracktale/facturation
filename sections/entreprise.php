<?php
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	## Profil entreprise
	#
	#	Routage des fonctionnalités du role entreprise
	#
	#	Attention ! Ce fichier ne doit jamais être édité
	
	$soustitre = "";
	
	switch($_SESSION["contexte"]["rubrique"])
	{
		default:
		case "accueil":
			$titre = "Accueil";
			include("entreprise_accueil.php");
			break;
			
		case "ventes":
			$titre = "Ventes";
			include("entreprise_ventes.php");
			break;
			
		case "client":
			$titre = "Clients";
			include("entreprise_client.php");
			break;
			
		case "articles":
			$titre = "Produits/services";
			include("entreprise_articles.php");
			break;
			
		case "preferences":
			$titre = "Préférences";
			include("entreprise_preferences.php");
			break;
			
	}
	if(isset($_GET["popin"])){$templateDir = "templates/";$templateFile = "popin.html"; $tmpVars = array("/{CONTENT}/i"); $replace = array($html); $page = new Page($templateDir, $templateFile, $_SESSION["locale"]);$page->replace_tags($tmpVars, $replace);echo $page->output();exit;}else{$html = $page->output();}	
	
	$templateDir = "templates/";
	$templateFile = "main_entreprise.html"; 
	$tmpVars = array("/{CONTENT}/i", "/{MENU_PRINCIPAL}/i", "/{TITRE}/i", "/{SOUSTITRE}/i", "/{INFOS_CONNEXION}/i", "/{REDIRECT}/i","/{META_TITLE}/i", "/{META_KEYWORDS}/i", "/{META_DESCRIPTION}/i", "/{BROADCAST}/i");
	$replace = array($html, $menu_principal, $titre, $soustitre, infos_connexion(), $_SERVER["REQUEST_URI"], $meta_title, $meta_keywords, $meta_description, $BROADCAST);
	$page = new Page($templateDir, $templateFile, $_SESSION["locale"]);
	$page->replace_tags($tmpVars, $replace);
	echo $page->output();
?>