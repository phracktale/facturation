<?php
	$page_name="";
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");
	$type_facture = 1;
	$type_avoir = 3;
	
	$piece = $T05->getPieceByID($type_facture, $_SESSION['user']['id_user'], $_GET['id']);
	
	$status = $T12->getStatusByPieceID($_GET['id'], $_SESSION['user']['id_user']);
	
	if($status[0]->T10_code_status_va == "ANNULE")
	{
		$BROADCAST = "Votre facture est déjà annulée, l'avoir correspondant est disponible";
	}
	else
	{
		if(count($piece) > 0)
		{
			$avoir = $T05->getNewNumOrder($_SESSION['user']['id_user'], $type_avoir);
			$numero_piece = "AV" . sprintf("%06d", $avoir[0]->num_piece);
			
			$codeinterne = $T05->insert(fr2mysqlDate(now('date')), $numero_piece, $_SESSION['user']['id_user'], $_GET['id'], $type_avoir, $piece[0]->T03_codeinterne_i, $piece[0]->T05_dateEcheance_d, $piece[0]->T05_intitule_va,  $piece[0]->T10_code_modeReglement_va,  $piece[0]->T05_remiseGlobale_n,  $piece[0]->T05_totalHT_n,  $piece[0]->T05_totalTVA_n,  $piece[0]->T05_totalTTC_n, $piece[0]->T05_lignes_va);
			
			if($codeinterne > 0)
			{
				$BROADCAST = "Votre facture  a été annulée, un avoir du même montant a été créé";
				$T12->insert($codeinterne);
				$T12->update("ANNULE", $_GET['id']);
			}
			else
			{
				$BROADCAST = "Votre facture n'a pu être annulée il y a eu un problème, merci de recommencer l'opération";
			}
		}
		else
		{
			$BROADCAST = "Cette facture n'existe pas qu'essayez vous de faire ?";
		}
	}
	$templateFile = "entreprise_ventes_facture_transferer.html";
	$tmpVars = array("/{CONTENT}/i");
	$replace = array($content);	
	$dynamic_vars = array();
	
?>