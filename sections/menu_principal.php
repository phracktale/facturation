<?php

 if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
 
	$menu = array (
					
				"public" => 	array (
											"item1"	=> array( 
																"code" => "accueil", 
																"class" => "", 
																"libelle" => _T("libMenuAccueil"),
																"sousmenu" => array(
													
															)
													)
					),
				"admin" => 	array (
											"item1"	=> array( 
																"code" => "accueil", 
																"class" => "", 
																"libelle" => _T("libMenuAccueil"),
																"sousmenu" => array(
													
															)
													)
					),
				"entreprise" => 	array (
											"item1"	=> array( 
																"code" => "accueil", 
																"class" => "", 
																"libelle" => _T("libMenuAccueil"),
																"sousmenu" => array(
													
															)
													),
											"item2"	=> array( 
																"code" => "ventes", 
																"class" => "", 
																"libelle" => _T("libMenuVentes"),
																"sousmenu" => array(
																				"item2-1" => array( "code" => "accueil", "class" => "", "libelle" => _T("libMenuVentesAccueil")),
																					"item2-2" => array( "code" => "facture_liste", "class" => "", "libelle" => _T("libMenuVentesFacture_liste")),
																					"item2-3" => array( "code" => "devis_liste", "class" => "", "libelle" => _T("libMenuVentesDevis_liste")),
																					"item2-4" => array( "code" => "avoir_liste", "class" => "", "libelle" => _T("libMenuVentesAvoir_liste")),
														
															)
													),
											"item3"	=> array( 
																"code" => "client", 
																"class" => "", 
																"libelle" => _T("libMenuClient"),
																"sousmenu" => array(
													
															)
													),
											"item4"	=> array( 
																"code" => "articles", 
																"class" => "", 
																"libelle" => _T("libMenuArticles"),
																"sousmenu" => array(
													
															)
													),
											"item5"	=> array( 
																"code" => "preferences", 
																"class" => "", 
																"libelle" => _T("libMenuPreferences"),
																"sousmenu" => array(
													
															)
													)
					)
	);
			
	
	
	if(isset($_SESSION["user"]["groupe"]))
	{
		
		
		$menu = $menu[$_SESSION["user"]["groupe"]];
		if(count($menu) > 0)
		{
			$menu_principal = "<nav id=\"menu\"><ul class=\"menu\">";
			foreach ($menu as $item)
			{
				$active = $item["code"] == $_SESSION["contexte"]["rubrique"] ? "on" : "off";
				$class = $item["class"] . " " . $active;
				$menu_principal .= "<li class=\"menu " . $item["code"] . " " . $class . "\"><a href=\"index.php?rubrique=" . $item["code"] . "\" class=\"button\"><span class=\"lib\">" . $item["libelle"] . "</span></a>";
				if(count($item["sousmenu"]) > 0)
				{
					$menu_principal .= "<ul class=\"sousmenu\">";
					foreach ($item["sousmenu"] as $ssitem)
					{
						$ssactive = $ssitem["code"] == $_SESSION["contexte"][$_SESSION["contexte"]["rubrique"]]["page"] ? "on" : "off";
						$class = $ssitem["class"] . " " . $ssactive;
						$menu_principal .= "<li class=\"" . $ssitem["code"] . " " . $class . "\"><a href=\"index.php?page=" . $ssitem["code"] . "\" class=\"button\"><span class=\"lib\">" . $ssitem["libelle"] . "</span></a></li>";
						
					}
				
					$menu_principal .= "</ul>";
				}
				$menu_principal .= "</li>";
			}
			$menu_principal .= "</ul></nav>";
		}
	}
?>