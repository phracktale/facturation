<?php
	$page_name="";
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");
	$type_piece = 2;
	
	$devis = $T05->getNewNumOrder($_SESSION['user']['id_user'], $type_piece);
	$num_devis = "DE" . sprintf("%06d", $devis[0]->num_piece);
	
	$piece = $T05->getPieceByID($type_piece, $_SESSION['user']['id_user'], $_GET['id']);
	
	$codeinterne = $T05->insert(fr2mysqlDate(now('date')), $num_devis, $_SESSION['user']['id_user'], 0, $type_piece, $piece[0]->T06_codeinterne_i, $piece[0]->T05_dateEcheance_d, $piece[0]->T05_intitule_va,  $piece[0]->T10_code_modeReglement_va,  $piece[0]->T05_remiseGlobale_n,  $piece[0]->T05_totalHT_n,  $piece[0]->T05_totalTVA_n,  $piece[0]->T05_totalTTC_n,  $piece[0]->T05_lignes_va);
	
	
	
	if($codeinterne > 0)
	{
		$BROADCAST = "Votre devis a été dupliqué il est sauvegardé en mode brouillon";
	}
	else
	{
		$BROADCAST = "Votre devis  n'a pas été dupliqué il y a eu un problème, merci de recommencer l'opération";
	}
	
	$templateFile = "entreprise_ventes_devis_dupliquer.html";
	$tmpVars = array("/{CONTENT}/i");
	$replace = array($content);	
	$dynamic_vars = array();
	
?>