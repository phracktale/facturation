<?php
	$page_name="";
	$oRetour = array();
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");
	$type_piece= 3; // Avoir
	$entete = $_POST['entete'];
	$lines = json_encode($_POST['lines']);
	
	if($entete['remise_globale'] == "")
	{
		$entete['remise_globale'] = 0;
	}

	/*
	print_r($_POST['entete']);
	print_r($_POST['lines']);
	*/
	
	
	$piece = $T05->getPieceByID($type_piece, $_SESSION['user']['id_user'], $entete['code_interne']);
	
	// Si la piece est présent dans la T05
	if(count($piece) > 0)
	{
		/*
		// On sauvegarde selon le mode de sauvegarde demandé
		// Brouillon ou valider
		// Un brouillon ne comporte pas de numéro de facture, il peut être modifié
		*/
		
		if($entete['mode_sauvegarde'] == "brouillon")
		{
			$T05->update($_SESSION['user']['id_user'], $type_piece, $entete['client_id'], fr2mysqlDate($entete['date_echeance']), $entete['intitule'], $entete['moyenReglement'], $entete['remise_globale'], $entete['total_ht'], $entete['total_tva'], $entete['total_ttc'], $lines, $entete['code_interne']);
			$T12->update("BROUILLON", $entete['code_interne']);
			$oRetour['code_interne'] = $entete['code_interne'];
		}
		else if($entete['mode_sauvegarde'] == "valider")
		{
			if ($piece[0]->T05_numPiece_va == "")
			{
				$facture = $T05->getNewNumOrder($_SESSION['user']['id_user'], $type_piece);
				$numero_piece = "AV" . sprintf("%06d", $facture[0]->num_piece);
				$oRetour['numero_piece'] = $numero_piece;
			}
			else
			{
				$numero_piece = $piece[0]->T05_numPiece_va;
				$oRetour['numero_piece'] = $numero_piece;
			}
			
			$T05->valider($numero_piece, $_SESSION['user']['id_user'], $type_piece, $entete['client_id'], fr2mysqlDate($entete['date_echeance']), $entete['intitule'], $entete['moyenReglement'], $entete['remise_globale'], $entete['total_ht'], $entete['total_tva'], $entete['total_ttc'], $lines, $entete['code_interne']);
			$T12->update("VALIDE", $entete['code_interne']);
		}
	}
	else
	{
		$codeinterne = $T05->insert(fr2mysqlDate(now('date')), "", $_SESSION['user']['id_user'], 0, $type_piece, $entete['client_id'], fr2mysqlDate($entete['date_echeance']), $entete['intitule'], $entete['moyenReglement'], $entete['remise_globale'], $entete['total_ht'], $entete['total_tva'], $entete['total_ttc'], $lines);
		$T12->insert($codeinterne);
		$oRetour['code_interne'] = $codeinterne;
	}
	
	echo json_encode($oRetour);
	
?>