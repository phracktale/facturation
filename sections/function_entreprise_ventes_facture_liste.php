<?php
	$page_name="";
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");
	$type_piece = 1;
	
	$factures = $T05->getPiecesByUser($type_piece, $_SESSION['user']['id_user']);
	
	if(count($factures) > 0 )
	{
		$liste_factures = '<table  class="datatable_pieces"><thead><tr><th class="w100p">Date</th><th class="w100p">Num fact</th><th class="w150p">Client</th><th class="w300p">Libellé</th><th class="w100p">Total HT</th><th class="w100p">Total TTC</th><th class="w100p">Etat</th><th class="w50p"></th><th class="w50p"></th><th class="w50p"></th><th class="w50p"></th></tr></thead><tbody>';
		foreach($factures as $facture)
		{
			if($facture->T03_raison_sociale_va != "")
			{
				$client_libelle = $facture->T03_raison_sociale_va;
			}
			else
			{
				$client_libelle = $facture->T03_contact_nom_va . " " . $facture->T03_contact_prenom_va;
			}
			
			$status = $T12->getStatusByPieceID($facture->T05_codeinterne_i, $_SESSION['user']['id_user']);
			$liste_factures .= '<tr><td>' . $facture->T05_datePiece_d . '</td><td>' . $facture->T05_numPiece_va . '</td><td>' . $client_libelle . '</td><td>' . $facture->T05_intitule_va . '</td><td class="txtright">' . sprintf("%02.2f", $facture->T05_totalHT_n, 2) . '</td><td class="txtright">' . sprintf("%02.2f", $facture->T05_totalTTC_n, 2) . '</td><td class="txtright">' . $status[0]->etat . '</td><td><input type="image" src="templates/images/icon_pdf.png" rel="facture|' . $facture->T05_numPiece_va . '|pdf" class="bouton export_piece" value="Export PDF" /></td><td><a href="?page=facture_editer&id=' . $facture->T05_codeinterne_i . '" title="Editer"><img src="templates/images/icon_editer.png" alt="Editer"  width="16" height="16"/></a></td><td><a href="?page=facture_transferer&id=' . $facture->T05_codeinterne_i . '" title="Annuler la facture et transférer en avoir"><img src="templates/images/icon_supprimer.png" alt="Annuler la facture et transférer en avoir" width="16" height="16"/></a></td><td><a href="?page=facture_dupliquer&id=' . $facture->T05_codeinterne_i . '" title="Dupliquer"><img src="templates/images/icon_dupliquer.png" alt="Dupliquer"  width="16" height="16"/></a></td></tr>';
		}
		$liste_factures .= '</tbody></table>';
	}
	
	
	$templateFile = "entreprise_ventes_facture_liste.html";
	$tmpVars = array("/{CONTENT}/i", "/{LISTE_FACTURES}/i");
	$replace = array($content, $liste_factures);	
	$dynamic_vars = array();
	
?>