<?php
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	

	switch($_SESSION["contexte"][$_SESSION["contexte"]["rubrique"]]["page"])
	{
		default:
		
		case "client_liste":
			include("function_entreprise_client_client_liste.php");
			break;
		case "client_editer":
			include("function_entreprise_client_client_editer.php");
			break;
		case "client_nouveau":
			include("function_entreprise_client_client_nouveau.php");
			break;
		case "client_supprimer":
			include("function_entreprise_client_client_supprimer.php");
			break;
		case "contact_liste":
			include("function_entreprise_client_contact_liste.php");
			break;
		case "contact_editer":
			include("function_entreprise_client_contact_editer.php");
			break;
		case "contact_nouveau":
			include("function_entreprise_client_contact_nouveau.php");
			break;
		case "contact_supprimer":
			include("function_entreprise_client_contact_supprimer.php");
			break;
	}
	
	$page = new Page($templateDir, $templateFile,  $_SESSION["locale"]);
	$page->setVars($dynamic_vars);
	
	$page->replace_tags($tmpVars, $replace);
	$html = $page->output();
	
?>