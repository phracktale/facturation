<?php
	$page_name="";
	$soustitre=_T("libSoustitre");
	$description = _T("libDescription");
	
	$type_piece = 3;
	
	$avoirs = $T05->getPiecesByUser($type_piece, $_SESSION['user']['id_user']);
	
	if(count($avoirs) > 0 )
	{
		$liste_avoirs = '<table  class="datatable_pieces"><thead><tr><th class="w100p">Date</th><th class="w100p">Num avoir</th><th class="w150p">Client</th><th>Libellé</th><th class="w100p">Total HT</th><th class="w100p">Etat</th><th class="w50p"></th><th class="w50p"></th><th class="w50p"></th></tr></thead><tbody>';
		foreach($avoirs as $avoir)
		{
			if($facture->T03_raison_sociale_va != "")
			{
				$client_libelle = $facture->T03_raison_sociale_va;
			}
			else
			{
				$client_libelle = $facture->T03_contact_nom_va . " " . $facture->T03_contact_prenom_va;
			}
			
			$status = $T12->getStatusByNumPiece($avoir->T05_numPiece_va, $_SESSION['user']['id_user']);
			
			$liste_avoirs .= '<tr><td>' . $avoir->T05_datePiece_d . '</td><td>' . $avoir->T05_numPiece_va . '</td><td>' . $client_libelle . '</td><td>' . $avoir->T05_intitule_va . '</td><td class="txtright">' . sprintf("%02.2f", $avoir->T05_totalTTC_n)  . '</td><td class="txtright">' . $status[0]->etat . '</td><td><input type="image" src="templates/images/icon_pdf.png" rel="avoir|' . $avoir->T05_numPiece_va . '|pdf" class="bouton export_piece" value="Export PDF" /></td><td><a href="?page=avoir_editer&id=' . $avoir->T05_codeinterne_i . '" title="Editer"><img src="templates/images/icon_editer.png" alt="Editer"  width="16" height="16"/></a></td><td><a href="?page=avoir_supprimer&id=' . $avoir->T05_codeinterne_i . '" title="Supprimer l\'avoir "><img src="templates/images/icon_supprimer.png" alt="Supprimer l\'avoir" width="16" height="16"/></a></td></tr>';
		}
		$liste_avoirs .= '</tbody></table>';
	}
	
	$templateFile = "entreprise_ventes_avoir_liste.html";
	$tmpVars = array("/{CONTENT}/i", "/{LISTE_AVOIRS}/i");
	$replace = array($content, $liste_avoirs);	
	$dynamic_vars = array();
	
?>